<?php
include 'header.php';
include 'admin/include/Database.inc.php';
?>
<div class="page-title">
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Blog</li>
			<li>Judul</li>
		</ol>
	</div>
	<h1>Judul</h1>
</div>
<section class="padding no-title text-center">
	<div class="container">
		<div class="row">
			<div class="the-blog col-md-9 col-sm-12">
				<?php
	                // get id from link
	                $id = intval($_GET['id']);
	                $query = mysqli_query($conn, "select * from blog where id=$id");
	                $baris = mysqli_fetch_array($query, MYSQL_ASSOC);
	                	echo"<div>";
	                	echo"<div class='col-md-12'>";
	                	echo"<img src='http://placehold.it/848x400' class='img-responsive'>";
	                	echo"</div>";
	                	echo"<div class='detail col-md-3 col-sm-12' style='float: right;'>";
	                	echo"<h5><b>Details</b></h5>";
	                	echo"<ul style='text-align: left;'>";
	                	echo"<li style='list-style-type: none'><i class='fa fa-user' aria-hidden='true'></i> " . $baris["author"] . "</li>";
	                	echo"<li style='list-style-type: none'><i class='fa fa-calendar' aria-hidden='true'></i> " . $baris["date"] . "</li>";
	                	echo"<li style='list-style-type: none'><i class='fa fa-tag' aria-hidden='true'></i> " . $baris["author"] . "</li>";
	                	echo"</ul>";
	                	echo"</div>";
	                	echo"<div class='isi col-md-9 col-md-12' style='float: left;'>";
	                	echo"<a href=''>";
	                	echo"<b>".strtoupper($baris['judul'])."</b><br><br>";
	                	echo"<p>".$baris['content']."</p>";
	                	echo"</a>";
	                	echo"</div>";
	                	echo"</div>";
                ?>
			</div>
			<div class="the-sidebar col-md-3 col-sm-12">
				<div class="category">
					<div class="cat-title">
						<p>CATEGORIES</p>
					</div>
					<div class="cat-list">
						<ul style="padding-left: 3%;">
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include 'footer.php';
?>