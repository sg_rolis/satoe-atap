<footer>
	  <div class="container-fluid">
	    <div class="row flex-row">
	      <div class="col-xs-12 col-sm-6 col-md-6 padding-box-70 blackbg dark">
	        <h3>Spot Seroja</h3>
	        <address>
	          Teras Balai Kelurahan Seroja<br>
	                    Jl. Seroja Barat No. 1<br>
	                    Selasa, 15.30 - 17.00 WIB<br>
	          <a class="bitly" href="https://www.google.co.id/maps/place/Kantor+Kelurahan+Karang+Kidul/@-6.989886,110.4239163,17z/data=!3m1!4b1!4m2!3m1!1s0x2e708b58a86a8bc7:0x3611fadf47911a6a">bit.ly/mapseroja</a><br>
	        </address>
	      </div>
	      <div class="col-xs-12 col-sm-6 col-md-6 padding-box-70 jetblackbg dark">
	        <h3>Spot Kelinci</h3>
	        <address>
	          Wisma Moerdiningsih<br>
	                    Jl. Gang Kelinci I No. 215<br>
	                    Rabu, 15.30 - 17.00 WIB<br>
	          <a  class="bitly" href="https://www.google.co.id/maps/place/Gg.+I,+Pandean+Lamper,+Gayamsari,+Kota+Semarang,+Jawa+Tengah+50249/@-6.9979481,110.4401832,17z/data=!3m1!4b1!4m5!3m4!1s0x2e708c96ff20498b:0x8ad8cd4a22519514!8m2!3d-6.9979481!4d110.4423719?hl=id">bit.ly/maptanggul</a><br>
	        </address>
	      </div>
	    </div>
	    <div class="row flex-row">
	    	<div class="col-xs-12 col-sm-6 col-md-6 padding-box-70 jetblackbg dark">
	        	<div class="widget widget-carlos-social-icons">
	       		<h5>Follow Us:</h5>
	           		<ul>
	             		<li>
	               			<a class="fb" href="https://www.facebook.com/satoeatap/" target="blank"><i class="fa fa-facebook"></i></a>
	             		</li>
	             		<li>
			               	<a class="yt" href="https://www.youtube.com/channel/UCn-FV8ZJSCPy1xRONWKj9nA" target="blank"><i class="fa fa-youtube"></i></a>
			            </li>
			            <li>
			            	<a class="tw" href="https://twitter.com/satoeatap" target="blank"><i class="fa fa-twitter"></i></a>
			            </li>
			            <li>
			            	<a class="ig" href="https://www.instagram.com/satoeatap/" target="blank"><i class="fa fa-instagram"></i></a>
			            </li>
	          		 </ul>
	           </div>
	      	</div>
		    <div class="col-xs-12 col-sm-6 col-md-6 padding-box-70 blackbg dark">
		        <div class="widget widget-carlos-social-icons">
		            <div class="widget widget-newsletter">
		     	       <p>Copyright © 2017 Satoe Atap</p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script> 
<!-- Date picker -->
<script src="http://rolies.me/client/sa/datepicker.js"></script>
<script src="http://rolies.me/client/sa/main.js"></script>


<script src="js/main.js"></script> 
<script src="js/smoothscroll.js"></script>
<!--Video Gallery-->
<script src="plugins/unitegallery-master/package/unitegallery/js/unitegallery.min.js" type='text/javascript'></script> 
<script src="plugins/unitegallery-master/package/unitegallery/themes/compact/ug-theme-compact.js" type='text/javascript'></script> 
<script src="js/wow.min.js"></script>
<!--When Element Visible-->
 <script src="js/jquery.inview.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script> 
<script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<!-- Light Box  -->
<script src="plugins/lightbox-master/dist/ekko-lightbox.js"></script>
<!--Parallax-->
<script src="plugins/parallax/js/jquery.easing.1.3.js"></script>
<script src="plugins/parallax/js/jquery.parallax-scroll.js"></script>
<!--CounterUp-->
<script src="plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script> 
<script src="plugins/counterup/waypoints.min.js" type="text/javascript"></script>
<!--Isotope Portfolio-->
<script src="plugins/isotope-portfolio/js/isotope.min.js"></script> 
<script src="plugins/isotope-portfolio/js/isotope-main.js"></script>
<!--Fancy Box-->
<script src="plugins/isotope-portfolio/js/jquery.fancybox.pack.js"></script>

<script src="js/jquery.dataTables.min.js"></script> <script src="js/matrix.tables.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
  
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs2.uzone.id/2fn7a2/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582CL4NjpNgssKWERuoCfisJekk6PcuCI%2b11JieNAgx1NVwnK7rsGrEiHFxwCXiS5MlqyKntDW6d2XS51yg48Qo7CtK8wJMC34qi2%2frFHWHcsxIkEg2k5Cl%2bpZGjGB3wIPYUNaWjdzZNiLElVxWyXKH1B1oecRnmp1ssyAhfsgQHtDuXL4ZppyNR0wpjH4npMmB%2b%2bU8onGmsoa9i7W2nSHOW15eTYENsaSJzolcwi84ke%2flntapNf9JhAZkEbt4nNuOOTEl9lJO9WnE0EsqumNYNjzXxjt9X6HIBmVfYxOiMF8%2f93wkAmJZRjywW83XhTbX3drsHx20ej2b%2f4zv3aQqAquIBl2U%2fzdxrjQb759qV8U3lCC0qLTw2Mjoo4DsLX%2f96m%2fRINtEWQUosQTK4Nkl49VO5c2uPerVp8akCDKRWKtironvyCjrAYNDG12b9YdYAxwZp%2fepPtjUQxOruvnjP0FVKWAmrmHk8ytLqc53FjU" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>

<!-- Mirrored from sys2netdemo.ca/carlos/about by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Jul 2017 06:15:14 GMT -->
</html>