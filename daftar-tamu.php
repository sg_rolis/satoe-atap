<?php
include 'header.php';
?>
<style>
	.form_coor input{
		width: 100%;
	}
	.form_coor textarea{
		width: 100%;
		height: 118px;
	}
	.form_coor input::placeholder,.form_coor textarea::placeholder{
		padding-left: 2%;
	}
	.tanggal{
		display: -webkit-box;
	}
</style>
  <div class="page-title">
		<div class="col-xs-12 breadcrumb-bar">
			<ol class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li>Guest List</li>
			</ol>
		</div>
		<h1>Guest List</h1>
	</div>
	<section class="padding">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<p>Harap di isi untuk para tamu yang datang di Satoe Atap.</p>
				</div>
			</div>
			<div class="row form_coor">
				<div class="xs-hide col-md-2">
				</div>
				<div class="col-md-8 col-xs-12">
				<form action="admin/input-tamu.php" method="post">
					<label>Nama</label><br>
					<input type="text" name="nama" placeholder="Nama" required><br><br>
					<label>Nomor HP</label><br>
					<input type="text" name="hp" placeholder="Nomor WA" required><br><br>
					<label>E-Mail</label><br>
					<input type="email" name="email" placeholder="E-Mail" required><br><br>
					<label>Nama Instansi / Komunitas</label><br>
					<input type="text" name="instansi" placeholder="Instansi/Komunitas" required><br><br>
					<label>Maksud Tujuan</label><br>
					<textarea name="maksud" placeholder="Maksud Tujuan" required=""></textarea>
					<center>
						<button type="submit" class="btn btn-primary" name="tamu" style="cursor: pointer;">Submit</button>
					</center>
				</form>
				</div>
			</div>
		</div>
	</section>
	<?php
	include 'footer.php';
	?>