<?php
include 'header.php';
?>
<div class="page-title">
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Donation</li>
			<li>Paper For Charity</li>
		</ol>
	</div>
	<h1>Paper For Charity</h1>
</div>



	<section class="padding">
		<div class="container">
            <div class="row">
            	<div class="col-md-6 wow bounceInBottom">
            	    <img class="img-responsive" src="img/pfc.jpg" alt="" />
            	</div>
                <div class="col-md-6" style="font-size: 22px">
                    <h1 class="theme no-top-margin marginbottom-30 effect-type"></h1>
                    <p>Jadi nanti kertas-kertas dan kardu-kardus yang sudah kakak berikan, akan kami jual. Dan dana yang diperoleh dari hasil penjualan akan dipergunakan untuk keperluan Satoe Atap seperti bayar sewa tempat atau untuk persiapan event yang akan datang.</p>
                    <p>Bagi kakak-kakak yang akan berdonasi #paperforcharity harap menghubungi nomor di bawah ini untuk janjian pengambilan barangnya. Bisa menghubungi SA Center ya kak.<br>
					Whatsapp : 089 570 444 8353 </p>
                </div>
            </div>
            
        </div>
	</section>
	
	<section class="portfolio-section port-col">
	    <div class="container">
	        <div class="row">
	            <div class="isotopeContainer">
	            <div class="col-sm-3 isotopeSelector art gallery">
	                <article class="">
	                    <figure>
	                        <img src="img/portfolio/1.jpg" alt="">
	                        <div class="overlay-background">
	                            <div class="inner"></div>
	                        </div>
	                        <div class="overlay">
	                            <div class="inner-overlay">
	                                <div class="inner-overlay-content with-icons">
	                                    <a Image" class="fancybox-pop" href="img/portfolio/17.jpg"><i class="fa fa-search"></i></a>
	                                </div>
	                            </div>
	                        </div>
	                    </figure>
	                </article>
	            </div>
	            
	            <div class="col-sm-3 isotopeSelector illustrations gallery">
	                <article class="">
	                    <figure>
	                        <img src="img/portfolio/2.jpg" alt="">
	                        <div class="overlay-background">
	                            <div class="inner"></div>
	                        </div>
	                        <div class="overlay">
	                            <div class="inner-overlay">
	                                <div class="inner-overlay-content with-icons">
	                                    <a Image" class="fancybox-pop" href="img/portfolio/2.jpg"><i class="fa fa-search"></i></a>
	                                </div>
	                            </div>
	                        </div>
	                    </figure>
	                </article>
	            </div>
	            
	            <div class="col-sm-3 isotopeSelector websites gallery">
	                <article class="">
	                    <figure>
	                        <img src="img/portfolio/3.jpg" alt="">
	                        <div class="overlay-background">
	                            <div class="inner"></div>
	                        </div>
	                        <div class="overlay">
	                            <div class="inner-overlay">
	                                <div class="inner-overlay-content with-icons">
	                                    <a Image" class="fancybox-pop" href="img/portfolio/3.jpg"><i class="fa fa-search"></i></a>
	                                </div>
	                            </div>
	                        </div>
	                    </figure>
	                </article>
	            </div>
	            
	            <div class="col-sm-3 isotopeSelector photography gallery">
	                <article class="">
	                    <figure>
	                        <img src="img/portfolio/4.jpg" alt="">
	                        <div class="overlay-background">
	                            <div class="inner"></div>
	                        </div>
	                        <div class="overlay">
	                            <div class="inner-overlay">
	                                <div class="inner-overlay-content with-icons">
	                                    <a Image" class="fancybox-pop" href="img/portfolio/3.jpg"><i class="fa fa-search"></i></a>
	                                </div>
	                            </div>
	                        </div>
	                    </figure>
	                </article>
	            </div>
	            </div>
	        </div>
	    </div>
	</section>
	
	<section class="padding no-title text-center">
		<div class="container video">
				<video controls data-animation="fadeInUp">
				  <source src="assets/paperforcharity.mp4" type="video/mp4">
				  <source src="assets/paperforcharity.ogg" type="video/ogg">
				  Your browser does not support HTML5 video.
				</video>
		</div>
	</section>
	
	
	<?php
	include 'footer.php';
	?>