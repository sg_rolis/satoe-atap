<?php
include 'header.php';
?>
<div class="page-title">
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
      <li><a href="index.html">Home</a></li>
      <li>About</li>
      <li>FAQ</li>
		</ol>
	</div>
	<h1>Frequently Asked Questions</h1>
</div>

	<section class="padding">
    <div class="container">
      <div class="row text-center">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left col-centered">
          <h3 class="marginbottom">Who We Are ?</h3>
          <div class="spacer"></div>
          <div class="panel-group" id="accordionOne" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionOne" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Apa Itu Satoe Atap ?
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" aria-labelledby="headingOne">
              <div class="panel-body">
                Satoe Atap adalah komunitas yang bergerak dibidang sosial pendidikan. Berbagi keceriaan dan pelajaran kepada anak-anak kaum miskin kota Semarang
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionOne" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Komunitas Ini Sudah Ada Sejak Kapan ?
                </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" aria-labelledby="headingTwo">
              <div class="panel-body">
                Satoe Atap sudah ada sejak tahun 2007 tanggal 12 April.
              </div>
            </div>
          </div>
        </div>
        <h3 class="marginbottom">Join Us</h3>
        <div class="spacer"></div>
        <div class="panel-group" id="accordionTwo" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading3">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionTwo" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                Gimana Cara Gabung Komunitas Ini ?
              </a>
            </h4>
          </div>
          <div id="collapse3" class="panel-collapse collapse in" aria-labelledby="heading3">
            <div class="panel-body">
              Langsung aja kakak datang ke pengajaran rutin kita kak :) dan kita juga tidak mengikat siapapun untuk selalu hadir di setiap acara, karna kita tahu kakak-kakak juga punya kesibukan sendiri-sendiri :)
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading4">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionTwo" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                Apakah Perlu Skill Khusus Untuk Bergabung?
              </a>
            </h4>
          </div>
          <div id="collapse4" class="panel-collapse collapse" aria-labelledby="heading4">
            <div class="panel-body">
              Ndak perlu skill atau keahlian apapun kok kak, yang terpenting mau mengajak adik-adik berinteraksi dan ramah ama mereka
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading5">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionTwo" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                Saya Ingin Bergabung Tapi Nanti Saya Paling Muda / Paling Tua Sendiri :(
              </a>
            </h4>
          </div>
          <div id="collapse5" class="panel-collapse collapse"  aria-labelledby="heading5">
            <div class="panel-body">
              Santai kak, volunteer Satoe Atap tidak ada batasan umur kok, mulai dari yang masih SMA sampai yang sudah berumah tangga ada kok kak, jadi rentan umur gajadi masalah, karna kita semua keluarga :)
            </div>
          </div>
        </div>
        <h3 class="marginbottom">Activity</h3>
        <div class="spacer"></div>
        <div class="panel-group" id="accordionThree" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading6">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionThree" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
                Pengajaran Tiap Hari Apa Saja ?
              </a>
            </h4>
          </div>
          <div id="collapse6" class="panel-collapse collapse in" aria-labelledby="heading6">
            <div class="panel-body">
              Pengajaran setiap hari Selasa dan Rabu pukul 15.30 - 17.00 WIB<br>
              Hari Selasa di <a target="blank" href="https://www.google.co.id/maps/place/Kantor+Kelurahan+Karang+Kidul/@-6.989886,110.4239163,17z/data=!3m1!4b1!4m2!3m1!1s0x2e708b58a86a8bc7:0x3611fadf47911a6a">Spot Seroja</a> dan untuk hari Rabu di <a target="blank" href="https://www.google.co.id/maps/place/Gg.+I,+Pandean+Lamper,+Gayamsari,+Kota+Semarang,+Jawa+Tengah+50249/@-6.9978802,110.4399984,17z/data=!3m1!4b1!4m5!3m4!1s0x2e708c96ff20498b:0x8ad8cd4a22519514!8m2!3d-6.9978802!4d110.4421871?hl=id">Spot Kelinci</a>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading7">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionThree" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                Kegiatan Satoe Atap Apa Aja Kak ?
              </a>
            </h4>
          </div>
          <div id="collapse7" class="panel-collapse collapse" aria-labelledby="heading7">
            <div class="panel-body">
              Kegiatan kita ada banyak kok kak, bisa dilihat di section '<a target="blank" href="activity.html">Activity</a>'' Kegiatan-kegiatan tersebut bisa diadakan atau tidak tergantung dari adakah yang bersedia menjadi penanggung jawab untuk kegiatan tersebut.
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading8">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionThree" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                Apakah Ada Jadwal Hari Ini Mau Belajar Tentang Apa ?
              </a>
            </h4>
          </div>
          <div id="collapse8" class="panel-collapse collapse"  aria-labelledby="heading8">
            <div class="panel-body">
              Kita tidak pernah mengadakan agenda akan memberikan materi khusus untuk adik-adik, semua kit lakukan secara spontan dan tanpa agenda (kecuali jika ada kerjasama). Jadi setiap pengajaran, kita langsung berbaur dengan adik-adik,  jika ada yang punya PR kita bantu, yang tidak punya PR bisa bermain sama kakak lainnya dan tidak mengganggu yang sedang beajar. Kalau ada kakak-kakak yang punya ide membuat kerajinan tangan bisa diajak adik-adiknya untuk membuat. Dari pada mengganggu yang lainnya.
            </div>
          </div>
        </div>
        <h3 class="marginbottom">Enviorement</h3>
        <div class="spacer"></div>
        <div class="panel-group" id="accordionFour" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading9">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionFour" href="#collapse9" aria-expanded="true" aria-controls="collapse9">
                Jumlah Anak-Anak Disini Ada Berapa?
              </a>
            </h4>
          </div>
          <div id="collapse9" class="panel-collapse collapse in" aria-labelledby="heading9">
            <div class="panel-body">
              Kalau dikumpulkan dari 2 spot pengajaran (Seroja dan Kelinci) bisa mencapai 80-100 anak kak. Buanyak banget yaa hehe. Kebayang ga gimana ramenya pas lagi ada suatu acara dan adik-adik nya banyak buanget, capek? iyalah hahaha tapi seruu
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading10">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionFour" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                Jumlah Kakak-Kakaknya Ada Berapa ?
              </a>
            </h4>
          </div>
          <div id="collapse10" class="panel-collapse collapse" aria-labelledby="heading10">
            <div class="panel-body">
              kalau untuk kakak volunteernya juga ada banyak, karna sudah sejak tahun 2007, tapi seiring berjalannya waktu mereka banyak yang bekerja di luar kota dan berumah tangga. Jadi untuk setiap pengajaran paling tidak ada 7 sampai 15 kakak yang hadir, banyak sih yang ingin hadir tapi bertabrakan dengan urusan pekerjaan kak
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading11">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionFour" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                Anak-Anaknya Seperti Apa Ya?
              </a>
            </h4>
          </div>
          <div id="collapse11" class="panel-collapse collapse"  aria-labelledby="heading11">
            <div class="panel-body">
              Ya namanya juga anak-anak kak hehe, pasti ada yang nurut ada yang bandel dan yang pastii mereka sangat hiperaktif kak
            </div>
          </div>
        </div>
        <h3 class="marginbottom">Colaboration</h3>
        <div class="spacer"></div>
        <div class="panel-group" id="accordionFive" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading12">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionFive" href="#collapse12" aria-expanded="true" aria-controls="collapse12">
                Saya Mau Mengadakan Kerjasama Nih, Gimana Ya Prosedurnya?
              </a>
            </h4>
          </div>
          <div id="collapse12" class="panel-collapse collapse in" aria-labelledby="heading12">
            <div class="panel-body">
              Bagi kakak-kakak yang ingin bekerjasama dengan Satoe Atap, bisa kontak SA Center (WA) +6289 570 444 8353
              Tapi alangkah baiknya kalau kakak-kakak mampir dulu ke pengajaran kita, jadi bisa ngobrol langsung kak, dan biar bisa tahu dulu kondisi adik-adiknya seperti apa hehe
            </div>
          </div>
        </div>
        <h3 class="marginbottom">Donation</h3>
        <div class="spacer"></div>
        <div class="panel-group" id="accordionSix" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="heading13">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionSix" href="#collapse13" aria-expanded="true" aria-controls="collapse13">
                Kalau Mau Donasi Berupa Barang Boleh Ndak?
              </a>
            </h4>
          </div>
          <div id="collapse13" class="panel-collapse collapse in" aria-labelledby="heading13">
            <div class="panel-body">
              Untuk donasi, kami menerima alat tulis, pakaian layak pakai, seragam dan perlengkapan anak-anak lainnya.
              Kakak bisa kontak dulu SA Center (WA) +6289 570 444 8353 untuk membahas kapan waktu penjemputan barang(wilayah Semarang) untuk wilayah di luar Semarang silahkan kontak SA Center untuk tujuan pengirimannya ya kak, atau mungkin kakak mau mengantarkannya langsung di hari pengajaran juga boleh kok :)
            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </section>
  <?php
  include 'footer.php';
  ?>