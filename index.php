<?php
include 'header.php';
?>
    <div id="main-slider">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
                    <div class="tp-banner-container">
                        <div class="tp-banner" >
                             <ul>   <!-- SLIDE  -->
                               <li class="text-center" data-transition="fade" data-slotamount="7" data-masterspeed="500"  data-thumb="" data-saveperformance="on"  data-title="">
                              <!-- MAIN IMAGE -->
                                <img class="img-responsive" src="img/dummy.png"  alt="slidebg1" data-lazyload="img/slider/slide-1.png" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!-- LAYERS --> 
                                
                                <div class="tp-caption custom_theme_big customin customout start"
                                data-x="center"
                                data-y="200"
                                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="600"
                                data-start="500"
                                data-easing="Back.easeOut"
                                data-endspeed="300"
                                data-endeasing="Power1.easeIn">
                                Satoe Atap
                                </div>  
                                <div class="fade tp-caption custom_theme_very_small customin customout start"
                                data-x="center"
                                data-y="320"
                                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="600"
                                data-start="500"
                                data-easing="Back.easeOut"
                                data-endspeed="300"
                                data-endeasing="Power1.easeIn">
                                Sayang Itou Asli Tanpa Pamrih
                                </div>
                                <div class="tp-caption customin customout start"
                                data-x="center"
                                data-y="360"
                                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="600"
                                data-start="500"
                                data-easing="Back.easeOut"
                                data-endspeed="300"
                                data-endeasing="Power1.easeIn">
                                <a class="btn btn-default btn-sm" href="#">About Us</a>
                                <a class="btn btn-default btn-sm" href="#">Our Work</a>
                                </div>
                              </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="padding-100-80">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-3 wow bounceInLeft">
                    <img class="img-responsive" src="img/SA.jpg" alt="" />
                </div>
                <div class="col-md-9 wow bounceInRight">
                    <h1>About Us</h1>
                    <p class="bold themecolor">Sayang Itoe Asli Tanpa Pamrih.</p>
                    <p>Satoe Atap merupakan komunitas yang memiliki filosofi “Sayang Itoe Asli Tanpa Pamrih” yang disingkat menjadi nama komunitas itu sendiri, Satoe Atap. Komunitas Satoe Atap berdiri sejak 12 April 2007 bertempat dan berkegiatan di Kota Semarang dan memiliki concern isu mengenai pendidikan terhadap anak jalanan dan kaum miskin kota. Komunitas Satoe Atap bersifat sosial, kekeluargaan, mandiri, dan independen dengan keanggotaan bersifat terbuka, dari seluruh kalangan dan elemen masyarakat.</p>
                    <div class="col-md-12 text-center margintop-30 wow fadeIn">
                        <a class="btn btn-primary" href="about.php">Read More</a>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-3" style="overflow: auto">
                <center><h3>Daftar Kerjasama</h3></center>
                <ul style="list-style: none">
                    <li>20 September 2017 UDINUS</li>
                    <li>20 September 2017 UDINUS</li>
                    <li>20 September 2017 UDINUS</li>
                    <li>20 September 2017 UDINUS</li>
                </ul>
            </div> -->
        </div>
    </section>
    <section class="container">
        <div class="col-md-9 wow bounceInRight">
            <h1>#paperforcharity</h1>
            <p>Kakak-kakak sekalian punya kertas bekas yang sudah tidak terpakai? Entah itu kertas koran, majalah, sisa-sisa revisi skripsi, kardus atau yang lainya. Yuk serahin ke kita-kita aja, biar jadi berkah..</p>
            <p class="bold themecolor">Kenapa Bia Jadi Berkah??</p>
            <div class="col-md-12 text-center margintop-30 wow fadeIn"> 
                <a class="btn btn-primary" href="paperforcharity.php">Read More</a> 
            </div>
        </div>
        <div class="col-md-3 wow bounceInLeft">
            <img class="img-responsive" src="img/pfc.jpg"/>
        </div>
    </section>
    <!-- <section class="padding">
        <div class="container">
            <div class="row">
                <div class="section-title wow zoomIn">
                    <h1>Carlos Creative Agency</h1>
                    <p>Morbi porta est a mi. Integer ut neque. Sed aliquet dictum turpis.</p>
                </div>
                <div class="col-md-4">
                    <div class="content-box style-seven wow fadeInLeft effect-border">
                        <h2>Creation</h2>
                        <p>Morbi semper, metus ac pur adipiscing, dui justo gravida arcu, et venenatis justo lacus ut nisi. Nunc tempus mauris et lectus. Pellentesque consectetuer, felis vel</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="content-box style-seven wow zoomIn effect-border">
                        <h2>Analysis</h2>
                        <p>Morbi semper, metus ac pur adipiscing, dui justo gravida arcu, et venenatis justo lacus ut nisi. Nunc tempus mauris et lectus. Pellentesque consectetuer, felis vel</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="content-box style-seven wow fadeInRight effect-border">
                        <h2>Strategy</h2>
                        <p>Morbi semper, metus ac pur adipiscing, dui justo gravida arcu, et venenatis justo lacus ut nisi. Nunc tempus mauris et lectus. Pellentesque consectetuer, felis vel</p>
                    </div>
                </div>
                
                
                
    

            </div>
        </div>
        <div class="parallax-element img1" data-parallax='{"y": 230, "smoothness": 5}'>
        <img src="img/3d/3d-box-1.png" alt=""/>
        </div>
        <div class="parallax-element img2" data-parallax='{"y": 50, "smoothness": 5}'>
        <img src="img/3d/3d-box-2.png" alt=""/>
        </div>
        <div class="parallax-element img3" data-parallax='{"y": 400, "smoothness": 5}'>
        <img src="img/3d/3d-box-3.png" alt=""/>
        </div>
    </section> -->
    

    

    <section id="about" class="padding scroll dark themebg wow  fadeIn" style="background-image:url(img/img-7.jpg)">
        <div class="container-fluid">
            <div class="row">
                <div class="section-title wow zoomIn">
                    <h1>Our Activity</h1>
                    <!-- <p>Morbi porta est a mi. Integer ut neque. Sed aliquet dictum turpis.</p> -->
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 no-padding style-eight-wrapper animate activity-box">
                    <div class="content-box style-eight wow fadeInLeft">
                    <i class="fa fa-globe"></i>
                        <h4>Reguler</h4>
                        <p>Kegiatan Pengajaran Rutin Setiap Hari Selasa Dan Rabu Pukul 15.30 - 17.00 WIB.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 no-padding style-eight-wrapper animate activity-box">
                    <div class="content-box style-eight wow fadeInUp">
                        <i class="fa fa-shopping-bag"></i>
                        <h4>Bazaar For Kids</h4>
                        <p>Bazaaaaaaaaaaarrr, ayoo ayooo diborong . . . .</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 no-padding style-eight-wrapper animate activity-box">
                    <div class="content-box style-eight wow fadeInDown">
                    <i class="fa fa-flag"></i>
                        <h4>17 Agustus</h4>
                        <p>17 Agustus Tahun 45 Itulah Hari Kemerdekaan Kitaaaa.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 no-padding style-eight-wrapper animate activity-box">
                    <div class="content-box style-eight wow fadeInRight">
                    <i class="fa fa-birthday-cake"></i>
                        <h4>Ulang Tahoen</h4>
                        <p>Selamaaaat Panjang Umur Dan Bahagiiiaaaaaa, Selamat Panjang Umur Dan Bahagiiiaaaaaa . . .</p>
                    </div>
                </div>
                <div class="col-md-12 text-center margintop-30 wow fadeIn"> <a class="btn btn-primary" href="activity.php">Show More</a> </div>
            </div>
        </div>
    </section>
    <section class="padding">
        <div class="container">
            <div class="row">
                <div class="section-title wow zoomIn">
                    <h1>Recruitment</h1>
                    <p class="featp">Bagi kakak-kakak yang ingin bergabung dikeluarga Satoe Atap, boleh banget kok. Syaratnya apa?</br>
                    Syaratnya SATOE aja kok kak,sayang sama anak-anak <i class="fa fa-smile-o"></i></br>
                    Kakak langsung aja datang ke tempat pengajaran rutin kita. </br>Kita tunggu ya kak!!</p>
                </div>
            </div>
        </div>        
    </section>
    <section class="padding cta style-one dark wow zoomIn">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h2>Let's Join Us And Make They Smile.</h2> </div>
                <div class="col-md-2"><a href="join-us.php" class="btn btn-info btn-lg">Join Us</a> </div>
            </div>
        </div>
    </section>
    <section class="padding-100-80">
        <div class="container">
            <div class="row">
                <center><h1>What They Say</h1></center><br>
                <div class="col-md-12 wow fadeInLeft">
                    <div class="carousel slide" data-ride="carousel" id="carousel-testimonial">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active testimonial">
                                <p>Setiap manusia itu unik, tak sama. Tapi tidak untuk saling mendiskriminasi. Berikan senyum ikhlasmu untuk setiap orang tanpa harus berharap balas.. semoga aku kamu dia dan mereka, akan menjadi kita dalam satoe atap 😁<br>
                                Nice to see you manteman barukuu.. terimakasi pengalaman berharga disetiap momennya 😄 jangan lupa bahagia.</p>
                                <div class="client-details"> 
                                <img class="img-responsive" src="assets/fetty.jpg" alt="" />
                                    <div>
                                        <h4>Fetty</h4> <span>Volunteer</span> </div>
                                </div>
                            </div>
                            <div class="item testimonial">
                                <p>Bergabung di satoeatap adalah untuk aku sendiri lebih melatih kesabaran "anak orang aja disayang apalagi anak sendiri" 😃 lalu di satoeatap sendiri tidak mengenal kata "senior dan junior" semua Sama, seperti Allah SWT, Dihadapan-NYA sama kecuali Iman dan Takwa....</p>
                                <div class="client-details"> 
                                <img class="img-responsive" src="assets/dhyas.jpg" alt="" />
                                    <div>
                                        <h4>Dhyas</h4> <span>Volunteer</span> </div>
                                </div>
                            </div>
                            <div class="item testimonial">
                                <p>Kesan:<br>
                                Selama bergabung dengan Satoe Atap saya merasa lebih bahagia karena bisa merasakan benar-benar apa itu arti dari "kebersamaan". Saya sangat bahagia saat melihat wajah adik-adik Satoe Atap riang bersemangat belajar dan bermain seolah tidak ada beban diantara mereka. Dengan adanya Satoe Atap ini saya merasa "kita adalah keluarga" yang tidak dipisahkan oleh usia dan status sosial, semua merangkul dan semua saling membahagiakan.
                                <br>Pesan:<br>
                                Semoga Satoe Atap senantiasa diberi kemudahan dalam menjalankan segala kegiatannya. Kakak-kakak dan adik-adik Satoe Atap senantiasa diberi kesehatan agar dapat selalu berkumpul bergembira bersama. Sukses terus Satoe Atap!</p>
                                <div class="client-details"> 
                                <img class="img-responsive" src="assets/shelvi.jpg" alt="" />
                                    <div>
                                        <h4>Shelvi</h4> <span>Volunteer</span> </div>
                                </div>
                            </div>
                            <div class="item testimonial">
                                <p>SA mengajarkan bahwa kebahagiaan tercipta bukan dari banyaknya materi, melainkan sahabat "mini" yang terus berdiri setulus hati memberi arti dikala sunyi lah sumber kebahagiaan hakiki..</p>
                                <div class="client-details"> 
                                <img class="img-responsive" src="assets/alif.jpg" alt="" />
                                    <div>
                                        <h4>Alif</h4> <span>Volunteer</span> </div>
                                </div>
                            </div>
                            <div class="item testimonial">
                                <p>Kesan: awalnya penasaran, akhirnya ketagihan<br>
                                   Pesan: semoga satoe atap tetap terus produktif dan volunteer maupun adek-adeknya nambah terus</p>
                                <div class="client-details"> 
                                <img class="img-responsive" src="assets/vera.jpg" alt="" />
                                    <div>
                                        <h4>Vera</h4> <span>Volunteer</span> </div>
                                </div>
                            </div>
                            <div class="item testimonial">
                                <p>Kesan: gaktau mau gimana lagi, suka banget sama satoe atap <br>
                                   Pesan: bisa ndak tangga muter-muternya diganti hehe</p>
                                <div class="client-details"> 
                                <img class="img-responsive" src="assets/khusna.jpg" alt="" />
                                    <div>
                                        <h4>Khusna</h4> <span>Volunteer</span> </div>
                                </div>
                            </div>
                            <div class="item testimonial">
                                <p>Kesan: sejak ikut Satoe Atap saya lebih mengerti arti kebersamaan yang ada dan juga bagaimana cara berbagi.<br>
                                   Pesan: semoga Satoe Atap tetap ada untuk berbagi kebahagiaan, tetap solid dan menjadi lentera untuk adik2 yang membutuhkan</p>
                                <div class="client-details"> 
                                <img class="img-responsive" src="assets/rizal.jpg" alt="" />
                                    <div>
                                        <h4>Rizal</h4> <span>Volunteer</span> </div>
                                </div>
                            </div>
                        </div>
                        <div class="btns"> <a class="btn btn-testimonials btn-sm" data-slide="prev" href="#carousel-testimonial" role="button"><span aria-hidden="true" class=
                            "fa fa-angle-left"></span> <span class=
                            "sr-only">Previous</span></a> <a class="btn btn-testimonials btn-sm" data-slide="next" href="#carousel-testimonial" role="button"><span aria-hidden="true" class=
                            "fa fa-angle-right"></span> <span class=
                            "sr-only">Next</span></a> </div>
                    </div>
                </div>
                <!-- <div class="col-md-6 wow fadeInRight"><img class="img-responsive" src="img/img-1.jpg" alt="" /> </div> -->
                <!-- <div class="col-md-7 wow fadeInRight carousel slide" id="carousel-example-generic" data-ride="carousel">
                  <ol class="carousel-indicators">
                      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                      <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  </ol>
                  <ul class="carousel-inner list-unstyled" role="listbox">
                    <li class="item active"><img src="img/img-1.jpg" alt=""/></li>
                    <li class="item"><img src="img/img-3.jpg" alt=""/></li>
                  </ul>
                </div> -->
            </div>
        </div>
        <div class="parallax-element img4" data-parallax='{"y": 300, "smoothness": 5}'>
        <img src="img/3d/3d-box-1.png" alt=""/>
        </div>
        <div class="parallax-element img7" data-parallax='{"y": 600, "smoothness": 5}'>
        <img src="img/3d/3d-box-4.png" alt=""/>
        </div>  
        
    </section>
    
    <section class="padding">
        <div class="container">
            <div class="row text-center wow zoomIn insta">
            <center>
                <!-- <iframe src="//users.instush.com/collage/?cols=7&rows=7&sl=true&user_id=1621736220&username=satoeatap&sid=-1&susername=-1&tag=-1&stype=mine&bg=transparent&space=true&rd=false&grd=true&gpd=6&drp=true&pin=false&t=999999VKPmYzCFDM66X8Mb2voscCOTs3G7h0qX6kWJ4CRm_qWN-1wDZ1JXcHwb8AB6mUFmbxtcNtZsUdk" allowtransparency="true" frameborder="0" scrolling="no"  style="display:block;width:733px;height:733px;border:none;overflow:visible;" ></iframe> -->
                <iframe src="http://instaembedder.com/gallery-v2.php?width=125&cols=7&rows=2&margin=2&color=gray&likes=1&date=1&frame=1&image_border=8&frame_color=white&cid=5584&" frameborder="0" width="1046" height="349" style="margin-left: 5%"></iframe>
            </center>
            </div>
        </div>
    </section>
    <?php
    include 'footer.php';
    ?>