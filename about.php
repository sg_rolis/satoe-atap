<?php
include 'header.php';
?>
<div class="page-title">
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>About</li>
			<li>About Us</li>
		</ol>
	</div>
	<h1>About Us</h1>
</div>



	<section class="padding">
		<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="theme no-top-margin marginbottom-30 effect-type">Vision</h1>
                    <p>Melakukan Pembinaan dan pemberdayaan anak jalanan dan kaum miskin kota yang selanjutnya disebut binaan di bidang pendidikan dan perekononmian demi menyongsong kehidupan masyarakat yang mandiri dan sejahtera sesuai dengan cita-cita bangsa Indonesia..</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 history">
                    <h1 class="theme no-top-margin marginbottom-30 effect-type">History</h1>
                    <p>Berawal dari niat baik beberapa mahasiswi Fakultas Hukum Universitas Diponegoro untuk memberikan sesuatu yang bermanfaat bagi anak-anak pemulung dan peminta di sekitar kampus Universitas Diponegoro pleburan, maka mereka bergerak mendekati anak-anak itu secara persuasif dan mengajak mereka belajar bersama. Berbekal ketulusan hati dan kebulatan tekad untuk sebuah perbaikan yang niscaya, anak-anak pemulung dan peminta pun bisa disentuh relung hatinya yang jua haus akan pendidikan. Mulai satu anak, dua anak hingga beberapa anak bergabung dengan gerakan sekelompok mahasiswi tersebut, kemudian mengisi perkumpulan mereka dengan belajar baca tulis, bermain yang sesuai usia anak, mendongeng, bernyanyi, menari.</p>
                    <p>Gerakan ini pun makin menarik perhatian mahasiswa-mahasiswa lain yang melihat kegiatan itu untuk turut serta, karena gerakan ini membuka mata mahasiswa-mahasiswa tersebut bahwa ada suatu tindakan nyata yang bisa dilakukan mahasiswa untuk perbaikan kondisi sosial masyarakat sekitar kampus tanpa harus berpeluh demonstrasi turun ke jalan. Sebagai puncaknya tanggal 12 April 2007, ke-10 mahasiswa Universitas Diponegoro sepakat mendirikan sebuah gerakan mahasiswa yang peduli pendidikan anak-anak jalanan dan anak-anak tidak mampu dengan nama SATOE ATAP (SAyang iTOE Asli TAnpa Pamrih) dengan sebuah impian memeratakan hak pendidikan bagi anak-anak tidak mampu sekitar kampus Universitas Diponegoro. Lalu, koordinator organisasi dipilih, struktur organisasi dibentuk, program kerja disusun, dan AD/ART organisasi dikukuhkan.</p>
                    <p>Gerakan Satoe Atap makin berkembang dan teratur dengan adanya pertemuan belajar rutin dua kali dalam seminggu di halaman masjid Diponegoro. Mengalirnya bantuan dana makin memantapkan langkah Satoe Atap dengan berbagai program yang dicitakan solutif bagi masyarakat miskin sekitar kampus. Maka, tak hanya rutinitas belajar yang Satoe Atap lakukan, namun juga memberikan beasiswa, dan program-program pelatihan keterampilan bagi adik binaan Satoe Atap (sebutan bagi anak-anak binaan Satoe Atap). </p>
                    <p>Manajemen organisasi dirancang sedemikian rupa hingga roda organisasi berjalan dengan baik, walaupun badai dan sandungan tak terelakkan dan harus dilalui dalam perjalanan Satoe Atap. Tak hanya program dan manajemen organisasi yang makin mengakar dan tumbuh dengan baik, jaringan dengan organisasi-organisasi lain yang sevisi, institusi-institusi yang mendukung misi Satoe Atap, dan pihak Universitas Diponegoro pun terjalin dengan anggun. Mahasiswa yang bergabung untuk ikut membangun Satoe Atap juga makin banyak, sehingga adik binaan yang bisa dirangkul Satoe Atap meningkat, bahkan keluarga adik binaan Satoe Atap juga turut diperhatikan karena Satoe Atap ingin berkontribusi secara komprehensif.</p>
                </div>
            </div>
        </div>
	</section>
	
	<!-- <section class="padding no-title blackbg dark">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 text-center animate">
				<div class="content-box style-four">
					<div class="icon animate"><i class="fa fa-file-movie-o"></i></div>
					<div class="separator"></div>
					<a href="#"><h4>Video Productions</h4></a>
					<p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid.</p>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 text-center animate">
				<div class="content-box style-four">
					<a href="#"><h4>Photography art</h4></a>
					<p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid.</p>
					<div class="separator"></div>
					<div class="icon animate"><i class="fa fa-camera"></i></div>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 text-center animate">
				<div class="content-box style-four">
					<div class="icon animate"><i class="fa fa-globe"></i></div>
					<div class="separator"></div>
					<a href="#"><h4>web development</h4></a>
					<p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid.</p>
				</div>
			</div>
				<div class="col-xs-12 col-sm-12 col-md-1">
				
				</div>
				<div class="col-xs-12 col-sm-12 col-md-5">
				<img class="img-responsive" src="img/img-5.jpg" alt=""/>
				</div>
			</div>
		</div>
	</section>
	
	<section class="padding no-title">
		<div class="container">
			
				
				<div class="row">
				
			
				
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="#" class="content-box style-one">
						<div class="icon animate">
						<i class="fa fa-bank"></i>
						</div>
						<h4>free updates</h4>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
					</a>	
				</div>
				
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="#" class="content-box style-one">
						<div class="icon animate">
						<i class="fa fa-paint-brush"></i>
						</div>
						<h4>creative theme</h4>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
					</a>	
				</div>
				
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="#" class="content-box style-one">
						<div class="icon animate">
						<i class="fa fa-forumbee"></i>
						</div>
						<h4>simple and clean</h4>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
					</a>	
				</div>
				
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="#" class="content-box style-one">
						<div class="icon animate">
						<i class="fa fa-eyedropper"></i>
						</div>
						<h4>corporate look</h4>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
					</a>	
				</div>
			
				
				
	
		</div>
		</div>
	</section>
	
	<section class="themebg padding">
	<div class="container">
		<div class="no-margin dark bs-callout-bold">
			<div class="left-border">
				<h4>The Creative template ever haha!</h4>	
				<p>tempus nunc. Ut bibendum et mauris id ullamcorper. Phasellus nec mollis ante. Etiam feugiat euismod ante sed convallis. Nam facilisis consequat elementum. Fusce vel felis congue est suscipit aliquet. Nunc dapibus risus quam, in laoreet velit ultricies vitae.</p>
			</div>
		</div>
	</div>
	</section> -->
<?php
include 'footer.php';
?>