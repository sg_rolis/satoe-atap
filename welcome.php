<?php
include 'header.php';
?>
<style>
	.form_coor input{
		width: 100%;
	}
	.form_coor textarea{
		width: 100%;
		height: 118px;
	}
	.form_coor input::placeholder,.form_coor textarea::placeholder{
		padding-left: 2%;
	}
	.tanggal{
		display: -webkit-box;
	}
</style>
  <div class="page-title">
		<div class="col-xs-12 breadcrumb-bar">
			<ol class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li>Guest List</li>
			</ol>
		</div>
		<h1>Guest List</h1>
	</div>
	<section class="padding">
		<div class="container">
			<div class="row form_coor">
				<form method="post" action="admin/input-tamu.php">
					<div class="xs-hide col-md-2">
					</div>
					<div class="col-md-8 col-xs-12">
						<center>
							<h2>Selamat Datang :)</h2><br><br>
							<a href="daftar-tamu.php"><div class="btn btn-primary" style="cursor: pointer;">Next</div></a>
						</center>
					</div>
				</form>
			</div>
		</div>
	</section>
	<?php
	include 'footer.php';
	?>