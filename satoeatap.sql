-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2017 at 10:12 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `satoeatap`
--

-- --------------------------------------------------------

--
-- Table structure for table `adik`
--

CREATE TABLE `adik` (
  `no` int(4) NOT NULL,
  `nama` varchar(222) NOT NULL,
  `jk` varchar(21) NOT NULL,
  `kelas` varchar(21) NOT NULL,
  `spot` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adik`
--

INSERT INTO `adik` (`no`, `nama`, `jk`, `kelas`, `spot`) VALUES
(1, 'Navis', 'Laki-Laki', '1 SMA/SMK', 'Seroja'),
(2, 'Sinta', 'Perempuan', '6 SD', 'Seroja'),
(3, 'Alfin', 'Laki-Laki', '4 SD', 'Seroja'),
(4, 'Fara', 'Perempuan', '6 SD', 'Seroja'),
(5, 'Aisyah', 'Perempuan', '6 SD', 'Seroja'),
(6, 'Astri Astuti', 'Perempuan', '5 SD', 'Seroja'),
(7, 'Dea Lytfa', 'Perempuan', '6 SD', 'Seroja'),
(8, 'Ika Oktavia', 'Perempuan', '5 SD', 'Seroja'),
(9, 'Kharisa Altea P', 'Perempuan', '3 SD', 'Seroja'),
(10, 'Ani Puspita Sari', 'Perempuan', '1 SMP', 'Seroja'),
(11, 'Iksani Budirianti', 'Perempuan', '6 SD', 'Seroja'),
(12, 'Mutia Hildafahriza', 'Perempuan', '4 SD', 'Seroja'),
(13, 'Adinda Aurora', 'Perempuan', '6 SD', 'Seroja'),
(14, 'Aira', 'Perempuan', 'TK', 'Tanggul'),
(15, 'Heru Setiawan', 'Laki-Laki', '3 SD', 'Tanggul'),
(16, 'Nur Visa Rahmawati', 'Perempuan', '3 SD', 'Tanggul'),
(17, 'Erni Purmitasari', 'Perempuan', '3 SD', 'Tanggul'),
(18, 'Meysia', 'Perempuan', '1 SD', 'Tanggul'),
(19, 'Sukma Setiani H', 'Perempuan', '3 SD', 'Tanggul'),
(20, 'Diah Eka', 'Perempuan', '3 SD', 'Tanggul'),
(21, 'Sifa Nuraini', 'Perempuan', '1 SD', 'Tanggul'),
(22, 'Safa Maheswari', 'Perempuan', '3 SD', 'Tanggul'),
(23, 'Nova', 'Perempuan', '2 SD', 'Tanggul'),
(24, 'Nurtri Yulianti', 'Perempuan', '3 SD', 'Tanggul'),
(25, 'Yulius Ibnu Saputra', 'Laki-Laki', '6 SD', 'Tanggul'),
(26, 'Novita Mahadewi', 'Perempuan', '2 SD', 'Tanggul'),
(27, 'Caka Faqiama', 'Laki-Laki', '2 SD', 'Tanggul'),
(28, 'M Febria Riski', 'Laki-Laki', '5 SD', 'Tanggul'),
(29, 'Fajar Nurintan H', 'Perempuan', '5 SD', 'Tanggul'),
(30, 'Evita Maharani', 'Perempuan', '5 SD', 'Tanggul'),
(31, 'Eva Mahasanti', 'Perempuan', '5 SD', 'Tanggul'),
(32, 'Muhammad Joy', 'Laki-Laki', '4 SD', 'Tanggul'),
(33, 'Ardi Bagas', 'Laki-Laki', '4 SD', 'Tanggul'),
(34, 'Anang Riski F', 'Laki-Laki', '5 SD', 'Tanggul'),
(35, 'Akbar Dwi K', 'Laki-Laki', '4 SD', 'Tanggul'),
(36, 'Yuda Dwi Kurniawan', 'Laki-Laki', '6 SD', 'Tanggul'),
(37, 'Eeyos Bambang Susislo', 'Laki-Laki', '1 SMP', 'Tanggul'),
(38, 'Alam Sigit P', 'Laki-Laki', '6 SD', 'Tanggul'),
(39, 'Sakti Bagus W', 'Laki-Laki', '3 SD', 'Tanggul'),
(40, 'M Singgih Soekarno', 'Laki-Laki', '1 SMP', 'Tanggul'),
(41, 'Bagas Adi Wilasa', 'Laki-Laki', '3 SMA/SMK', 'Tanggul'),
(42, 'Hendi Purnomo', 'Laki-Laki', 'TK', 'Seroja'),
(43, 'Bagas Galih S', 'Laki-Laki', '1 SD', 'Seroja'),
(44, 'Eka Sutanto Prasetyo', 'Laki-Laki', '1 SD', 'Seroja'),
(45, 'Roy Arizal', 'Laki-Laki', '1 SMP', 'Tanggul'),
(46, 'Aan Nurwahyudi', 'Laki-Laki', '6 SD', 'Tanggul'),
(47, 'Septian Hadi', 'Laki-Laki', '5 SD', 'Tanggul'),
(48, 'Laili Dwi I', 'Perempuan', '2 SMP', 'Tanggul'),
(49, 'Indra Aji P', 'Laki-Laki', '-', 'Tanggul'),
(50, 'Dafa Uku P', 'Laki-Laki', '3 SD', 'Tanggul'),
(51, 'Arisa Nuraisa', 'Perempuan', '4 SD', 'Seroja'),
(52, 'Surys Ningrum', 'Perempuan', '5 SD', 'Seroja'),
(53, 'Tru Cahaya S', 'Perempuan', '3 SD', 'Seroja'),
(54, 'Nurjana Cahya Sari', 'Perempuan', '3 SD', 'Seroja'),
(55, 'Gery', 'Laki-Laki', '1 SD', 'Seroja'),
(56, 'Vani Andrianto', 'Laki-Laki', '1 SMP', 'Seroja'),
(57, 'Bima KUrniaji', 'Laki-Laki', '2 SD', 'Seroja'),
(58, 'Riski Widyaputra', 'Laki-Laki', '4 SD', 'Seroja'),
(59, 'Muhammad Subkhi', 'Laki-Laki', '5 SD', 'Seroja'),
(60, 'Yoga Febri P', 'Laki-Laki', '6 SD', 'Tanggul'),
(61, 'Ajeng Putri Herita', 'Perempuan', '3 SD', 'Tanggul'),
(62, 'M Wahyu Satria P', 'Laki-Laki', 'Tidak Sekolah', 'Tanggul');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `no` int(3) NOT NULL,
  `Title` varchar(222) NOT NULL,
  `Author` varchar(55) NOT NULL,
  `Date` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `no` int(5) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `role` varchar(12) NOT NULL,
  `lastlogin` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`no`, `nama`, `username`, `password`, `role`, `lastlogin`) VALUES
(1, 'Edo', 'raidou', 'raidou', 'admin', ''),
(2, 'Rolies', 'rolies', 'rolies', 'admin', ''),
(3, 'Cepi', 'cepi', 'cepi', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `volunteer`
--

CREATE TABLE `volunteer` (
  `no` int(11) NOT NULL,
  `nama` varchar(233) NOT NULL,
  `hp` varchar(233) NOT NULL,
  `tahun_gabung` varchar(5) NOT NULL,
  `jk` varchar(21) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `volunteer`
--

INSERT INTO `volunteer` (`no`, `nama`, `hp`, `tahun_gabung`, `jk`) VALUES
(1, 'Edo Erdian Firmansyah', '081225113357', '2015', 'Laki-Laki'),
(2, 'Alan', '089653781810', '2017', 'Laki-Laki'),
(3, 'Alif', '089626027767', '2017', 'Laki-Laki'),
(4, 'Ara', '085766563804', '2017', 'Perempuan'),
(5, 'Fetty Dyah', '085801617101', '2017', 'Perempuan'),
(6, 'Amanda Happy Diana', '083838690822', '2017', 'Perempuan'),
(7, 'Vera', '085799371804', '2017', 'Perempuan'),
(8, 'Nihazah Siti', '085800763163', '2016', 'Laki-Laki'),
(9, 'Amel', '085740663560', '2017', 'Perempuan'),
(10, 'Amelia', '085601883022', '2017', 'Perempuan'),
(11, 'Arisa', '085643797660', '2017', 'Perempuan'),
(12, 'Roro Ayu Probo', '08112734583', '2017', 'Perempuan'),
(13, 'Cita', '085727926808', '2017', 'Perempuan'),
(14, 'Dhyas', '082220102820', '2017', 'Perempuan'),
(15, 'Fatma', '081249603871', '2017', 'Perempuan'),
(16, 'Mufti', '085741369194', '2017', 'Perempuan'),
(17, 'Rizka', '085641459167', '2016', 'Perempuan'),
(18, 'Aris', '085641707797', '2017', 'Laki-Laki'),
(19, 'Awaludin Rizky', '085712799269', '2011', 'Laki-Laki'),
(20, 'Hari Kus', '08562709700', '2011', 'Laki-Laki'),
(21, 'Jaswadi', '081226615353', '2011', 'Laki-Laki'),
(22, 'Jordi', '081228508941', '2017', 'Laki-Laki'),
(23, 'Nia', '081213013488', '2017', 'Perempuan'),
(24, 'Sasa', '082227477080', '2017', 'Perempuan'),
(25, 'Nuvita', '082322237366', '2017', 'Perempuan'),
(26, 'Ridlo Putuismaya', '085640085965', '2016', 'Laki-Laki'),
(27, 'Rizal', '082145783914', '2017', 'Laki-Laki'),
(28, 'Shelvi', '082247331009', '2017', 'Perempuan'),
(29, 'Arisky Yudha N', '082138167600', '2017', 'Laki-Laki');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adik`
--
ALTER TABLE `adik`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`no`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
