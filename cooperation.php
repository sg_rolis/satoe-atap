<?php
include 'header.php';
?>
<style>
	.form_coor input{
		width: 100%;
	}
	.form_coor textarea{
		width: 100%;
		height: 118px;
	}
	.form_coor input::placeholder,.form_coor textarea::placeholder{
		padding-left: 2%;
	}
	.docs-datepicker .btn-lg {
		font-size: 12px;
		margin-bottom: 0;
		margin-top: 6px;
		margin-left: 12px;
	}
	.input-group {width: 60%}
	.input-group .btn {
		border: none!important;
		margin-bottom: 0;
		background: transparent!important;
		color: #AAA!important;
	}
	.input-group-btn{
		border: 1px solid #aaaaaa;
		border-left: none;
		background: #E9ECEF;
	}
	.input-group label {
		display: block;
	}
</style>
  <div class="page-title">
		<div class="col-xs-12 breadcrumb-bar">
			<ol class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li>About</li>
				<li>Cooperation</li>
			</ol>
		</div>
		<h1>Cooperation</h1>
	</div>
	<section class="padding">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<p>Untuk kakak-kakak yang ingin mengadakan kerja sama dengan Satoe Atap harap mengisi data dibawah ini ya, acara maksimal 7 hari sebelum data ini di isi.</p>
				</div>
			</div>
			<div class="row form_coor">
				<hr>
				<form method="post" action="">
					<div class="col-md-6 col-xs-12">
						<label>Nama Instansi / Komunitas</label><br>
						<input class="form-control" type="text" name="instansi" placeholder="Instansi/Komunitas" required><br><br>
						<label>Penanggung Jawab</label><br>
						<input class="form-control" type="text" name="pj" placeholder="Penanggung Jawab" required><br><br>
						<label>Nomor WA</label><br>
						<input class="form-control" type="text" name="wa" placeholder="Nomor WA" required><br><br>
						<label>Deskripsi Kegiatan</label><br>
						<textarea class="form-control" name="deskripsi" placeholder="Deskripsi" required></textarea>
					</div>
					<div class="col-md-6 col-xs-12">
						<div class="docs-datepicker">
							<label>Tanggal</label><br>
						  	<div class="input-group pull-left">
							    <input type="text" placeholder="Pick a date" name="date-start" class="form-control docs-date">
							    <span class="input-group-btn">
							      <button class="btn btn-default docs-datepicker-trigger" type="button">
							        <i aria-hidden="true" class="fa fa-calendar"></i>
							      </button>
							    </span>
						 	</div>
						  	<div class="docs-datepicker-container"></div>
						  	<a class="btn btn-danger btn-lg" data-toggle="collapse" href="#sampai">Lebih Dari Sehari</a>
						</div>
						<br>
						<br>
						<div id="sampai" class="collapse">
							<div class="docs-datepicker">
								<label>Sampai</label><br>
							  	<div class="input-group">
								    <input type="text" placeholder="Pick a date" name="date-end" class="form-control docs-date">
								    <span class="input-group-btn">
								      <button class="btn btn-default docs-datepicker-trigger" type="button">
								        <i aria-hidden="true" class="fa fa-calendar"></i>
								      </button>
								    </span>
							 	</div>
							  	<div class="docs-datepicker-container"></div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="row padding">
				<div class="col-md-12 text-center">
					<button class="btn btn-success btn-lg">Request Cooperation</button>
				</div>
			</div>
		</div>
	</section>
	<?php
	include 'footer.php';
	?>