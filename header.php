<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from sys2netdemo.ca/carlos/about by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Jul 2017 06:15:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
	<link rel="icon" type="image/png" href="img/logoSA.png">	
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Satoe Atap - Sayang Itoe Asli Tanpa Pamrih</title>
	<meta name="Satoe ATap" content="komunitas,sosial,pendidikan,anak,semarang"/>
	<meta name="keywords" content="komunitas,sosial,pendidikan,anak,semarang" />
	<meta name="google-site-verification" content="OkXZhPKiO5bArQYLt-nPnF98AtKWZMJh5dE8Upu4aEk" />
    
    <!-- Merchandise -->
    <link href="css/Mstyle.css" rel="stylesheet">

    <link rel="stylesheet" href="css/matrix-style.css" />
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,700" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="http://rolies.me/client/sa/datepicker.css" rel="stylesheet">
    <!-- Style -->
    <link href="style.css" rel="stylesheet">
    <!-- Responsive -->
    <link href="css/responsive.css" rel="stylesheet">
    <!-- FontAwesome -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="plugins/rs-plugin/css/extralayers.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="plugins/rs-plugin/css/settings.css" media="screen" />
	<!-- Light Box -->
	<link href="plugins/lightbox-master/dist/ekko-lightbox.css" rel="stylesheet">	
    <!-- AnimateCSS -->
    <link href="plugins/animate/animate.css" rel="stylesheet">
	<!-- Isotope -->
    <link href="plugins/isotope-portfolio/css/isotope.css" rel="stylesheet">
	<!-- Fancy Box -->
    <link href="plugins/isotope-portfolio/css/jquery.fancybox.css" rel="stylesheet">
	<!--Video Gallery-->
    <link rel='stylesheet' href="plugins/unitegallery-master/package/unitegallery/css/unite-gallery.css" type='text/css' />
    <!-- Custom Style -->
    <link href="custom_style.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-100415391-2', 'auto');
	  ga('send', 'pageview');

	</script>
</head>

<body>
    <!-- Fixed navbar -->
    <nav id="main-menu" class="navbar navbar-inverse navbar-fixed-top" data-spy="affix" data-offset-top="100">
        <div class="container">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <a class="navbar-brand" href="index.html">Satoe Atap</a> </div>
            <div class="navbar-collapse collapse navbar-right" id="navbar">
			<ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">About <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="about.php">About Us</a></li>
                <li><a href="data-adik.php">Children</a></li>
                <li><a href="cooperation.php">Cooperation</a></li>
                <li><a href="faq.php">FAQ</a></li>
            </ul>
            <li><a href="activity.php">Activity</a></li>
            <li><a href="join-us.php">Join Us</a></li>
            <li><a href="blog.php">Blog</a></li>
            <li><a href="souvenir.php">Merchandise</a></li>
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Donation <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="donation.php">Donate</a></li>
                <li><a href="report.php">Report</a></li>
                <li><a href="paperforcharity.php">Paper For Charity</a></li>
            </ul>
            <li><a href="contact.php">Contact Us</a></li>
        </ul>
                
            </div>
			
            <!--/.nav-collapse -->
        </div>
    </nav>