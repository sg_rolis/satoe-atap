<?php
include 'header.php';
?>
  <div class="page-title">
		<div class="col-xs-12 breadcrumb-bar">
			<ol class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li>Contact</li>
			</ol>
		</div>
		<h1>Contact Us</h1>
	</div>
	<section class="padding">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<h2>Send us message</h2>
					<p>Untuk kakak-kakak yang mempunyai pertanyaan tentang Satoe Atap, bisa menghubung kami melalui berbagai cara yang tersedia disini ya kak.</p>
				</div>
			</div>
			<div class="row">
				<div class=" col-md-4 col-xs-12">
					<div class="widget-title">
						<h4>SA Center</h4>
					</div>
					<p>Tel : <a href="">+6289 570 444 8353</a></p>
					<p>WA : <a href="">+6289 570 444 8353</a></p>
				</div>
				<div class=" widget-carlos-social-icons col-md-4 col-xs-12">
					<div class="widget-title">
						<h4>Social Media:</h4>
					</div>
		            <ul style="margin-top: 2%">
		                <li>
		                    <a class="fb" href="https://www.facebook.com/satoeatap/" target="blank"><i class="fa fa-facebook"></i></a>
		                </li>
		                <li>
		                    <a class="yt" href="https://www.youtube.com/channel/UCn-FV8ZJSCPy1xRONWKj9nA" target="blank"><i class="fa fa-youtube"></i></a>
		                </li>
		                <li>
		                    <a class="tw" href="https://twitter.com/satoeatap" target="blank"><i class="fa fa-twitter"></i></a>
		                </li>
		                <li>
		                    <a class="ig" href="https://www.instagram.com/satoeatap/" target="blank"><i class="fa fa-instagram"></i></a>
		                </li>
		            </ul>
		        </div>
		        <div class=" col-md-4 col-xs-12">
					<div class="widget-title">
						<h4>Email</h4>
					</div>
					<p>Email : <a href="">sapasatoeatap@gmail.com</a></p>
				</div>
		    </div>
		</div>
	</section>
	<?php
	include 'footer.php';
	?>