<?php
include 'header.php';
?>
<div class="page-title">
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Join US</li>
		</ol>
	</div>
	<h1>Join Us</h1>
</div>

		
		<!-- 4 Column Portfolio -->



<section class="padding join-us">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xm-12">
                <center>
                <i class="fa fa-map-marker"></i>
                <h5>Spot Seroja</h5>
                <a target="blank" href="https://www.google.co.id/maps/place/Kantor+Kelurahan+Karang+Kidul/@-6.989886,110.4239163,17z/data=!3m1!4b1!4m2!3m1!1s0x2e708b58a86a8bc7:0x3611fadf47911a6a">Map Seroja</a>
                </br>
                Selasa 15.30 - 17.00 WIB</br>Teras Balai Kelurahan Seroja
                </center>
            </div>
            <div class="col-md-6 col-xm-12 text-center">
                <p class="join-p">Ingin bergabung jadi keluarga Satoe Atap? Hayuuuk kak <i class="fa fa-smile-o"></i></br>
                Syarat nya apa? Syaratnya cuma SATOE kakak, sayang sama anak-anak ajah. Sayang sama kakak-kakaknya menyusul gapapa kok.
                </br>
                Caranya? Kakak tinggal datang aja langsung ke pengajaran rutin kami.</br>
                Kuy!! Berbagi keceriaan dan kebahagiaan bersama adik-adik binaan Satoe Atap.
                Karena . . .</p></br><h2>SAYANG ITOE ASLI TANPA PAMRIH</h2>
            </div>
            <div class="col-md-3 col-xm-12">
                <center>
                <i class="fa fa-map-marker"></i>
                <h5>Spot Kelinci</h5>
                <a target="blank" href="https://www.google.co.id/maps/place/Gg.+I,+Pandean+Lamper,+Gayamsari,+Kota+Semarang,+Jawa+Tengah+50249/@-6.9978802,110.4399984,17z/data=!3m1!4b1!4m5!3m4!1s0x2e708c96ff20498b:0x8ad8cd4a22519514!8m2!3d-6.9978802!4d110.4421871?hl=id">Map Kelinci</a>
                </br>
                Rabu 15.30 - 17.00 WIB</br>Wisma Moerdiningsih
                </center>
            </div>
        </div>
    </div>        
</section>

<section class="filter-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
            
            	<h2 class="sr-only">Filter</h2>
           
                <div class="filter-container isotopeFilters">
                    <ul class="list-inline filter">
                        <li class="active"><a href="#" data-filter="*">All </a><span>/</span></li>
                        <li><a href="#" data-filter=".seroja">Spot Seroja</a><span>/</span></li>
                        <li><a href="#" data-filter=".tanggul">Spot Tanggul</a><span>/</span></li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
</section>



<section class="portfolio-section port-col">
    <div class="container">
        <div class="row">
            <div class="isotopeContainer">
            
            <div class="col-sm-3 isotopeSelector seroja gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/srj1.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/srj1.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector seroja gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/srj2.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/srj2.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector seroja gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg1.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg1.jp1g"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector tanggul gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/klc1.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/klc1.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector seroja gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg4.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg4.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector tanggul gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg2.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg2.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector tanggul gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg3.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg3.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector seroja gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg5.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg5.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector tanggul gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/klc2.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/klc2.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector tanggul gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/klc3.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/klc3.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector seroja gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg6.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg6.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector tanggul gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/klc4.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/klc4.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>
            
            </div>
        </div>
    </div>
</section>
<?php
include 'footer.php';
?>