<?php
include 'header.php';
include 'admin/include/Database.inc.php';
?>
<div class="page-title">
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Blog</li>
		</ol>
	</div>
	<h1>Blog</h1>
</div>
<section class="padding no-title text-center">
	<div class="container">
		<div class="row">
			<div class="the-blog col-md-9 col-sm-12">
				<?php
				$query = mysqli_query($conn, "select * from blog");
				// == PAGINATION START HERE
				// define how many item in one page
				$results_per_page = 2;

				// Find the number blog stored in database
				$number_of_article =  mysqli_num_rows($query);
				$number_of_pages = ceil($number_of_article/$results_per_page);

				// Determine which page number visitor currently on
				if (!isset($_GET['page'])) {
					$page = 1;
				}else {
					$page = $_GET['page'];
				}

				// Determine the sql start limit on each page
				$page_first_limit = ($page-1) * $results_per_page;

				// Retriving data based on selected result
				$sql = "SELECT * FROM blog where status ='approve' LIMIT " . $page_first_limit . ',' . $results_per_page;
				$result = mysqli_query($conn, $sql);

                //END PAGINATION
                while ($baris = mysqli_fetch_array($result)) {
                ?>
               	<div class="content-blog">
	            	<div class="col-md-12">
	            		<img src="http://placehold.it/848x400" class="img-responsive">
	            	</div>
	            	<div class="detail col-md-3 col-sm-12 pull-right">
		            	<h5><b>Details</b></h5>
		            	<ul style="text-align: left;">
			            	<li style="list-style-type: none"><i class="fa fa-user" aria-hidden="true"></i><?php echo $baris["author"]; ?></li>
			            	<li style="list-style-type: none"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo $baris["date"]; ?></li>
			            	<li style="list-style-type: none"><i class="fa fa-tag" aria-hidden="true"></i><?php echo $baris["author"]; ?></li>
		            	</ul>
	            	</div>
	            	<div class="isi col-md-9 col-md-12 pull-left">
		            	<b><?php echo strtoupper($baris["judul"]); ?></b>
		            	<p><?php echo substr($baris["content"],0,190)."..."; ?></p>
		            	
			            <a href="single-blog.php?id=<?php echo $baris["id"]; ?> " class="btn btn-primary" style='cursor: pointer'>Read More</a>
		            </div>
            	</div>
         		<?php
         		} 
         		?>
         		<div class="col-sm-12">
         			<?php 
         			for ($page=1; $page<=$number_of_pages ; $page++) { 
         					echo "<a href='blog.php?page=$page' class='btn btn-link'>".$page."</a>";
         				}
         			 ?>
         		</div>
			</div>
			<div class="the-sidebar col-md-3 col-sm-12" style="margin-top: 2%">
				<div class="category">
					<div class="cat-title">
						<p>CATEGORIES</p>
					</div>
					<div class="cat-list">
						<ul style="padding-left: 3%;">
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
							<li><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="">Pendidikan (2)</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include 'footer.php';
?>