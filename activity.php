<?php
include 'header.php';
?>
<div class="page-title">
	
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Activity</li>
		</ol>
	</div>
	<h1>Our Activities</h1>
</div>

<section class="padding activity">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xm-12 text-center">
                <p class="acti-p">
                Kegiatan reguler yang rutin diadakan 2 kali dalam seminggu di 2 titik pengajaran Satoe Atap 
                Spot Seroja pada hari Selasa dan Spot Tanggul pada hari Rabu 
                Pukul 15.30 sampai 17.00 WIB
                </br>
                    <div class="col-md-6 text-center">
                        <i class="fa fa-map-marker"></i>
                        <br>
                        <h2>Spot Seroja</h2><br>
                        <a href="https://www.google.co.id/maps/place/Kantor+Kelurahan+Karang+Kidul/@-6.989886,110.4239163,17z/data=!3m1!4b1!4m2!3m1!1s0x2e708b58a86a8bc7:0x3611fadf47911a6a">Map Seroja</a>
                    </div>
                    <div class="col-md-6 text-center">
                        <i class="fa fa-map-marker"></i>
                        <br>
                        <h2>Spot Kelinci</h2><br>
                        <a href="https://www.google.co.id/maps/place/Gg.+I,+Pandean+Lamper,+Gayamsari,+Kota+Semarang,+Jawa+Tengah+50249/@-6.9978802,110.4399984,17z/data=!3m1!4b1!4m5!3m4!1s0x2e708c96ff20498b:0x8ad8cd4a22519514!8m2!3d-6.9978802!4d110.4421871?hl=id">Map Kelinci</a>
                    </div>
                </p>
            </div>
        </div>
    </div>        
</section>

<section class="padding activity1">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xm-12 text-center">
                <p class="acti-p">
                    <h2>Bazar For Kids</h2>
                    Nah dari barang-barang yang sudah kakak-kakak sumbangkan, nantinya akan dipergunakan untuk kegiatan Bazar For Kids nih kak. Disini adik-adik akan diberikan sejumlah koin yang nantinya akan mereka gunakan untuk membeli barang-barang yang dijual. Adik-adik juga diajarkan supaya mengenal skala prioritas, barang mana yang mereka butuhkan dan barang mana yang kurang mereka butuhkan. 
                    Kegiatan ini dilaksanakan setahun sekali.
                </p>
            </div>
            <div class="col-md-6 col-xm-12 text-center">
                <p class="acti-p">
                    <h2>17 Agustusan</h2>
                    Dulu kakak-kakak pasti pernah ikut lomba 17-an di RT/RW nya kan yes? Adik-adik nya juga mau dong kak, yuk kita meriahkan suasana kemerdekaan 17 Agustus dengan lomba-lomba yang seru barnge adik-adik binaan Satoe Atap. 
                    Kegiatan ini dilaksanakan saat bulan kemerdekaan Negara Kesatuan Republik Indonesia.<br>
                    <h2>MERDEKA!!!</h2>
                </p>
            </div>
        </div>
    </div>        
</section>

<section class="padding activity2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xm-12 text-center">
                <p class="acti-p">
                    <h2>Hompimpah</h2>
                    Hompimpah alaihum gambreng!! Pasti tau kan kalimat tersebut hehe
                    Diambil dari nama permainan tradisional, Satoe Atap mengadakan keseruan yang lebih seru. Tapi kali ini adik-adik binaan Satoe Atap enggak ikut kak. Trus siapa dong?
                    Kali ini Satoe Atap mengajak adik-adik di luar Satoe Atap agar juga bisa merasakan kecerian dan kebaagian yang sama dengan adik-adik binaan Satoe Atap. Disini adik-adik juga akan diberikan pembekalan softskill selama 2 hari.
                    Bagi kakak-kakak yang tertarik untuk ikutan Hompimpah, kegiatan ini diadakan 2 tahun sekali aja lho kak,jadi terus kepoin sosial media kita yes.
                </p>
            </div>
            <div class="col-md-6 col-xm-12 text-center">
                <p class="acti-p">
                    <h2>Ulang Tahun</h2>
                   Tiup lilinnya tiup lilinnya sekarang juga, sekaraaaang juugaaa, sekaraaaang juuugaaaaa.
                   Yeaay, Satoe Atap Ulang tahun lho kak. Tanggal berapa coba tebak? hayooo? Tingtong! bener banget 12 April.
                   Disini kita bakal ada keceriaan dan keseruan yang super duper seru lho kak. Pokoknya seru deh, buat kakak-kakak taun lalu ga sempet datang, tahun depan jangan lupa datang yuk kak.
                </p>
            </div>
        </div>
    </div>        
</section>

<section class="filter-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
            
            	<h2 class="sr-only">Filter</h2>
           
                <div class="filter-container isotopeFilters">
                    <ul class="list-inline filter">
                        <li class="active"><a href="#" data-filter="*">All </a><span>/</span></li>
                        <li><a href="#" data-filter=".reguler">Reguler</a><span>/</span></li>
                        <li><a href="#" data-filter=".bfk">Bazar For Kids</a><span>/</span></li>
                        <li><a href="#" data-filter=".17ags">17 Agustus</a><span>/</span></li>
                        <li><a href="#" data-filter=".homp">Hompimpah</a><span>/</span></li>
                        <li><a href="#" data-filter=".hut">Ulang Tahun</a></li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
</section>



<section class="portfolio-section port-col">
    <div class="container">
        <div class="row">
            <div class="isotopeContainer">
            
            <div class="col-sm-3 isotopeSelector reguler gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg1.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg1.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector 17ags gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/ags1.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/ags1.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector 17ags gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/ags2.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/ags2.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector reguler gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg2.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg2.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector 17ags gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/ags3.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/ags3.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector reguler gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg3.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg3.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector bfk gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/bfk1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/bfk1.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector homp gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/hom3.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/hom3.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector reguler gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg4.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg4.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector bfk gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/bfk2.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/bfk2.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector reguler gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg5.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg5.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector homp gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/hom4.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/hom4.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector reguler gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/reg6.jpeg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/reg6.jpeg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector bfk gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/bfk3.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/bfk3.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector 17ags gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/ags5.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/ags5.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector bfk gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/bfk4.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/bfk4.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector bfk gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/bfk5.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/bfk5.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector homp gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/hom1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/hom1.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector hut gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/1.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/17.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector homp gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/hom2.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/hom2.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            <div class="col-sm-3 isotopeSelector 17ags gallery">
                <article class="">
                    <figure>
                        <img src="img/activity/ags4.jpg" alt="">
                        <div class="overlay-background">
                            <div class="inner"></div>
                        </div>
                        <div class="overlay">
                            <div class="inner-overlay">
                                <div class="inner-overlay-content with-icons">
                                    <a Image" class="fancybox-pop" href="img/activity/ags4.jpg"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class="article-title"><a href="#"></a></div>
                </article>
            </div>

            
            
            
            </div>
        </div>
    </div>
</section>
<?php
include 'footer.php';
?>