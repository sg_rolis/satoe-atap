<?php
include 'sidebar.php';
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#"> Data Cooperation</a><a href="#" class="current">Input Data</a> </div>
    <h1>Input Data Cooperation</h1>
  </div>
  <div class="container-fluid">
    <hr>
    	<form action="inputcoopP.php" method="post">
	        <label>Tanggal</label>
	        <br>
    		<select name="tanggal" style="width: 64px">
			  <option value="01">1</option>
			  <option value="02">2</option>
			  <option value="03">3</option>
			  <option value="04">4</option>
			  <option value="05">5</option>
			  <option value="06">6</option>
			  <option value="07">7</option>
			  <option value="08">8</option>
			  <option value="09">9</option>
			  <option value="10">10</option>
			  <option value="11">11</option>
			  <option value="12">12</option>
			  <option value="13">13</option>
			  <option value="14">14</option>
			  <option value="15">15</option>
			  <option value="16">16</option>
			  <option value="17">17</option>
			  <option value="18">18</option>
			  <option value="19">19</option>
			  <option value="20">20</option>
			  <option value="21">21</option>
			  <option value="22">22</option>
			  <option value="23">23</option>
			  <option value="24">24</option>
			  <option value="25">25</option>
			  <option value="26">26</option>
			  <option value="27">27</option>
			  <option value="28">28</option>
			  <option value="29">29</option>
			  <option value="30">30</option>
			  <option value="31">31</option>
			</select>
			<select name="bulan" style="width: 122px">
			  <option value="January">Januari</option>
			  <option value="February"Februari></option>
			  <option value="March">Maret</option>
			  <option value="April">April</option>
			  <option value="May">Mei</option>
			  <option value="June">Juni</option>
			  <option value="July">Juli</option>
			  <option value="August">Agustus</option>
			  <option value="September">September</option>
			  <option value="October">Oktober</option>
			  <option value="November">November</option>
			  <option value="December">Desember</option>
			</select>
			<select name="tahun" style="width: 85px">
			  <option value="2017">2017</option>
			  <option value="2018">2018</option>
			</select>
    		<br>
    		<br>
    		<label>Instansi</label>
    		<input type="text" name="instansi" placeholder="Instansi" required><br>
    		<br>
    		<label>Kegiatan</label><br>
    		<textarea name="kegiatan" style="width: 278px;height: 200px" required></textarea>
    		<br>
    		<label>Spot</label><br>
    		<input type="radio" name="spot" value="Seroja" checked> Seroja<br>
			<input type="radio" name="spot" value="Kelinci"> Kelinci<br>
			<br>
    		<label>PenanggungJawab</label><br>
    		<input type="text" name="PJ" placeholder="PJ" required><br>
    		<br>
    		<label>Kontak</label><br>
    		<input type="text" name="kontak" placeholder="Kontak" required><br>
    		<br>
    		<label>PenanggungJawab SA</label><br>
    		<input type="text" name="PJSA" placeholder="PJSA" required><br>
    		<br>
			<br><br>
     	    <input type="submit" name="submit" value="Submit">
    	</form>
  </div>
</div>
<?php
include 'footer.php';
?>