<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Login to dashboard satoe atap</title>
    <title>Satoe Atap</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <style>
        .login-container {
            width: 440px;
            margin: 0 auto;
            background: #8CCDEE;
            border-radius: 8px;
            position: relative;
            margin-top: 5em;
            padding: 25px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <?php
                // $pass = 'cepi';
                // $hashed = password_hash($pass, PASSWORD_BCRYPT);
                // if (password_verify($pass, $hashed)) {
                //     echo "Verified <br>";
                // }
                // echo $hashed;
            ?>
            <div class="login-container">
                <form action="include/login.inc.php" method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                    </div>
                    <button type="submit" name="sign" class="btn btn-primary">Login</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>