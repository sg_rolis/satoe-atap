<?php
  /*******************************************************
   * Only these origins will be allowed to upload images *
   ******************************************************/
  
  /*********************************************
   * Change this line to set the upload folder *
   *********************************************/
  $DT = date("Y-m-d h:i:sa", strtotime("+5 hours"));
  $Array = explode(" ",$DT);
  $dates=$Array[0];
  $times=$Array[1];
  
  $ar1 = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
  $ar2 = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
  $ar3 = array('1','2','3','4','5','6','7','8','9','0');

  $a1=$ar1[rand(0,25)];
  $a2=$ar2[rand(0,25)];
  $a3=$ar3[rand(0,9)];
  $a4=$ar2[rand(0,25)];
  $a5=$ar1[rand(0,25)];
  $a6=$ar3[rand(0,9)];
  $a7=$ar2[rand(0,25)];

  $imageFolder = "imgpost/".$a1.$a2.$a3.$a4.$a5.$a6.$a7;

  reset ($_FILES);
  $temp = current($_FILES);
  if (is_uploaded_file($temp['tmp_name'])){
    if (isset($_SERVER['HTTP_ORIGIN'])) {
      
    }

    /*
      If your script needs to receive cookies, set images_upload_credentials : true in
      the configuration and enable the following two headers.
    */
    // header('Access-Control-Allow-Credentials: true');
    // header('P3P: CP="There is no P3P policy."');

    // Sanitize input
    if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
        header("HTTP/1.0 500 Invalid file name.");
        return;
    }

    // Verify extension
    if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
        header("HTTP/1.0 500 Invalid extension.");
        return;
    }

    // Accept upload if there was no origin, or if it is an accepted origin
    $filetowrite = $imageFolder . $temp['name'];
    move_uploaded_file($temp['tmp_name'], $filetowrite);

    // Respond to the successful upload with JSON.
    // Use a location key to specify the path to the saved image resource.
    // { location : '/your/uploaded/image/file'}
    echo json_encode(array('location' => $filetowrite));
  } else {
    // Notify editor that the upload failed
    header("HTTP/1.0 500 Server Error");
  }
?>
