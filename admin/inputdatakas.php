<?php
include 'sidebar.php';
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Kas</a><a href="#" class="current">Input Data</a> </div>
    <h1>Input Data Kas</h1>
  </div>
  <div class="container-fluid">
    <hr>
    	<form method="post" action="inputdatakasP.php">
    		<label>Bulan</label>
    		<select name="month" style="width: 220px">
			  <option value="Januari">Januari</option>
			  <option value="Februari">Februari</option>
			  <option value="Maret">Maret</option>
			  <option value="April">April</option>
			  <option value="Mei">Mei</option>
			  <option value="Juni">Juni</option>
			  <option value="Juli">Juli</option>
			  <option value="Agustus">Agustus</option>
			  <option value="September">September</option>
			  <option value="Oktober">Oktober</option>
			  <option value="November">November</option>
			  <option value="Desember">Desember</option>
			</select>
			<br>
			<label>Tahun</label>
		    <select name="year" style="width: 220px">
		      <!-- <option value="2016">2016</option> -->
		      <option value="2017">2017</option>
		    </select>
		    <br>
    		<label>Keterangan</label>
    		<input type="text" name="desc" placeholder="Keterangan" required>
		    <br>
    		<label>Pemasukan</label>
    		<input type="text" name="income" placeholder="Pemasukan" required>
		    <br>
    		<label>Pengeluaran</label>
    		<input type="text" name="outcome" placeholder="Pengeluaran" required>
			<br><br>
     	    <input type="submit" name="submit" value="Submit">
    	</form>
  </div>
</div>
<?php
include 'footer.php';
?>