<?php
include 'sidebar.php';
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="#" class="current">Input User</a> </div>
    <h1>Input New User</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row">
    	<div class="col-md-6">
    		<div class="container">
    		  <form action="inputuserP.php" method="POST">
    		    <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="user" name="user" placeholder="Username" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kelas" class="col-sm-3 col-form-label">Role</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="role" id="role">
                            <option value="user">User</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>

    		    <br>
    		    <div class="form-group row">
    		      	<div class="col-sm-12">
    		      		<button type="submit" class="btn btn-primary" name="submit" style="cursor: pointer;">Submit</button>
    		      	</div>
    		    </div>
    		  </form>
    		</div>
    	</div>
    </div>
  </div>
</div>
<?php
include 'footer.php';
?>