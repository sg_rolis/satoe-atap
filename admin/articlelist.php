<?php
include 'sidebar.php';
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#"> Article</a><a href="#" class="current">Article List</a> </div>
    <h1>Show Data Adik</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Article List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Date</th>
                  <th>Status</th>
                  <?php
                  if($_SESSION["role"]=='admin'){
                    echo"<th>Action</th>";
                  }
                  ?>
                </tr>
              </thead>
              <tbody>
                <?php
                error_reporting(1);
                $koneksi = mysql_connect("localhost","root","") or die("Koneksi Gagal !" . mysql_error());
                $db = mysql_select_db("satoeatap") or die("Database tidak ada !" . mysql_error());
                //Query
                $no=0;
                $query = mysql_query("select * from blog", $koneksi);
                while ($baris = mysql_fetch_array($query, MYSQL_ASSOC)) {
                  $no++;
                  echo "<tr>";
                  echo "<td>" . $no . "</td>";
                  echo "<td class='img-adik'><center>";
                    if($baris['image_name'] == ""){
                      echo"<a href=''><img src='../img/SA.jpg'></a></center></td>";
                    }
                    else{
                      echo"<a href=''><img src='getPhotoBlog.php?id=".$baris['id']."'></a></center></td>";
                    }
                  echo "<td>" . $baris["judul"];
                  if($_SESSION['role'] == 'admin'){
                  ?>
                  <form method='post' action='edit-blog.php' style='float:right'>
                  <input type='text' name='id' value='<?php echo $baris["id"] ?>' style='display:none'>
                  <button type='submit' class='btn btn-info btn-lg'>Edit</button>
                  </form>
                  </td>
                  <?php
                  }
                  else{
                    if($_SESSION['nama'] == $baris['author']){
                      echo "<form method='post' action='edit-blog.php' style='float:right'>
                          <input type='text' name='id' value='".$baris["id"]."' style='display:none'>
                          <button type='submit' class='btn btn-info btn-lg' style='cursor: pointer'>Edit</button>
                          </form>";
                    }
                  }
                  echo "<td>" . $baris["author"] . "</td>";
                  echo "<td>" . $baris["date"] . "</td>";
                  if($baris["status"]=='approve'){
                      echo "<td><span style='color:green'>Approve</span></td>";
                  }
                  else{
                      echo "<td><span style='color:red'>Pending</span></td>";
                  }
                  if($_SESSION["role"]=='admin'){
                  ?>
                  <td>
                    <form method='post' action='action-article.php' style='float:right'>
                      <input type='text' name='id' value='<?php echo $baris["id"] ?>' style='display:none'>
                      <button type='submit' class='btn btn-info btn-lg' style='cursor: pointer'>Action</button>
                    </form>
                  </td>
                  <?php
                  }
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include 'footer.php';
?>