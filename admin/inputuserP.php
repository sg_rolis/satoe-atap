<?php
session_start();
include 'include/Database.inc.php';
$query = mysqli_query($conn, "select * from user");
$num_rows = mysqli_num_rows($query);

if(isset($_POST['submit'])){
	$no = $num_rows+2;
	$nama = $_POST['nama'];
	$user = $_POST['user'];
	$pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);
	$role = $_POST['role'];
	
	$sql = "INSERT INTO user (no, nama, username, password, role, lastlogin) VALUES ($no,'$nama','$user','$pass','$role','')";

	if (mysqli_query($conn, $sql)) {
	    echo "New record created successfully";
	    header("index.php?input=success");

	} else {
	    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}

	mysqli_close($conn);

	header("Location: index.php?input=success");
}
elseif(isset($_POST['edit'])){
	$id = $_SESSION['no'];
	$nama = $_POST['nama'];
	$user = $_POST['user'];
	$pass = $_POST['pass'];

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "satoeatap";
	$conn = mysqli_connect($servername, $username, $password, $dbname);

	$hashedPwdCheck = password_verify($pass, $_SESSION['pass']);
	if($hashedPwdCheck == false){
		echo "<script>
			window.location.href='index.php';
			alert('Maaf Password Anda Salah');
			</script>";
	} 
	elseif($hashedPwdCheck == true) {
		$sql = "update user set nama='$nama',username='$user' where no=$id";
		mysqli_query($conn,$sql);
		

		if (mysqli_query($conn, $sql)) {
		    header("Location: index.php?input=success");

		} 
		else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}

		mysqli_close($conn);

		header("Location: index.php?input=success");
	}
}
else{//change password
	$id = $_SESSION['no'];
	$passOld = $_POST['passO'];
	$passNew = $_POST['passN'];

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "satoeatap";
	$conn = mysqli_connect($servername, $username, $password, $dbname);

	$hashedPwdCheck = password_verify($passOld, $_SESSION['pass']);
	if($hashedPwdCheck == false){
		echo "<script>
			window.location.href='index.php';
			alert('Maaf Password Anda Salah');
			</script>";
	} 
	elseif($hashedPwdCheck == true) {
		$hashed = password_hash($passNew, PASSWORD_BCRYPT);
		$sql = "update user set password='$hashed' where no=$id";
		mysqli_query($conn,$sql);
		

		if (mysqli_query($conn, $sql)) {
		    header("Location: index.php?input=success");

		} 
		else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}

		mysqli_close($conn);

		header("Location: index.php?input=success");
	}
}
?>