<?php
include 'sidebar.php';
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#"> User List</a></div>
    <h1>User List</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>User List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Action</th>
                  <th>Role</th>
                  <th>Last Login</th>
                </tr>
              </thead>
              <tbody>
                <?php
                error_reporting(1);
                $koneksi = mysql_connect("localhost","root","") or die("Koneksi Gagal !" . mysql_error());
                $db = mysql_select_db("satoeatap") or die("Database tidak ada !" . mysql_error());
                //Query
                $query = mysql_query("select * from user", $koneksi);
                while ($baris = mysql_fetch_array($query, MYSQL_ASSOC)) {
                  echo "<tr>";
                  echo "<td>" . $baris["nama"] . "</td>";
                  echo "<td>" . $baris["username"] . "</td>";
                  echo "<td>" . $baris["password"] . "</td>";
                  echo "
                  <td>
                    <form method='post' action='edit-user.php' style='float:right'>
                      <input type='text' name='id' value='".$baris["no"]."' style='display:none'>
                      <button type='submit' class='btn btn-info btn-lg'>Edit</button>
                    </form>
                  </td>";
                  echo "<td>" . $baris["role"] . "</td>";
                  echo "<td>" . $baris["lastlogin"] . "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include 'footer.php';
?>