<?php
include 'sidebar.php';
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#"> Souvenir</a><a href="#" class="current">Input Item</a> </div>
        <h1>Input Item</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row">
            <div class="col-md-6">
                <form action="include/form/inputitemsP.php" method="post">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nama" name="name" placeholder="Nama" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="harga" class="col-sm-3 col-form-label">Harga</label>
                        <div class="col-sm-9">
                             <input type="text" id="harga" class="form-control" name="price" placeholder="Harga" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="stock" class="col-sm-3 col-form-label">Stock</label>
                        <div class="col-sm-9">
                             <input type="text" id="stock" class="form-control" name="stock" placeholder="Stok" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content-item">Deskripsi</label>
                        <textarea class="form-control" id="content-item" name="info" rows="6" placeholder="Konten"></textarea>
                    </div>
                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include 'footer.php';
?>