<?php
  session_start();
  if (empty($_SESSION['username'])) {
      // JIka belum login
    header("Location: ../admin/login.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Satoe Atap</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link rel="stylesheet" href="css/uniform.css" />
<link rel="stylesheet" href="css/select2.css" />
<link rel="stylesheet" href="css/matrix-style.css" />
<link rel="stylesheet" href="css/matrix-media.css" />
<link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style.css" />
<!-- editor -->
<script src="//cdn.ckeditor.com/4.7.2/full/ckeditor.js"></script>
<script src="texteditor/ckeditor.js"></script>
<script src="texteditor/sample.js"></script>
<link rel="stylesheet" href="texteditor/samples.css">
<link rel="stylesheet" href="texteditor/neo.css">
<!--  -->
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<script>
tinymce.init({
  selector: '#mytextarea',
  height:'350',
  images_upload_url: 'post.php',
  images_upload_credentials: true,
  plugins: [
    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
    'save table contextmenu directionality emoticons template paste textcolor'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
});
tinymce.activeEditor.uploadImages(function(success) {
  $.post('post.php', tinymce.activeEditor.getContent()).done(function() {
  console.log("Uploaded images and posted content as an ajax request.");
  });
});
</script>
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="">Satoe Atap</a></h1>
</div>
<!--close-Header-part--> 

<!--sidebar-menu-->

<div id="sidebar" style="position: fixed">
  <ul>
    <li class="sidebar-active"> <a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="sidebar-active"> <a href="userlist.php"><i class="icon icon-user"></i> <span>User List</span></a> </li>
    <?php
    if($_SESSION['role'] == "admin"){
    ?>
      <li class="sidebar-active submenu"> <a href=""><i class="fa fa-money"></i> <span>Kas</span> <span class="label label-important">2</span></a>
        <ul>
          <li><a href="inputdatakas.php">Input Data</a></li>
          <li><a href="data-kas.php">Show Data</a></li>
        </ul>
      </li>
    <?php 
    }
    ?>
    <?php
    if($_SESSION['role'] == "admin"){
    ?>
      <li class="sidebar-active submenu"> <a href="#"><i class="fa fa-users"></i> <span>Data Adik</span> <span class="label label-important">2</span></a>
        <ul>
          <li><a href="inputdataadik.php">Input Data</a></li>
          <li><a href="data-adik.php">Show Data</a></li>
        </ul>
      </li>
      <li class="sidebar-active submenu"> <a href=""><i class="fa fa-users" aria-hidden="true"></i> <span>Cooperation</span> <span class="label label-important">2</span></a>
        <ul>
          <li><a href="inputdatacoop.php">Input Data</a></li>
          <li><a href="data-coop.php">Show Data</a></li>
        </ul>
      </li>
      <li class="sidebar-active submenu"> <a href=""><i class="fa fa-users"></i> <span>Volunteers</span> <span class="label label-important">2</span></a>
        <ul>
          <li><a href="inputdatavolunteer.php">Input Data</a></li>
          <li><a href="data-volunteer.php">Show Data</a></li>
        </ul>
      </li>
      <li class="sidebar-active submenu"> <a href="#"><i class="icon icon-file"></i> <span>Article</span> <span class="label label-important">2</span></a>
        <ul>
          <li><a href="writearticle.php">Write Article</a></li>
          <li><a href="articlelist.php">Article List</a></li>
        </ul>
      </li>
      <li class="sidebar-active submenu"> <a href="#"><i class="fa fa-shopping-cart"></i> <span>Merchandise</span> <span class="label label-important">2</span></a>
        <ul>
          <li><a href="items.php">Input Items</a></li>
          <li><a href="showitems.php">Show Items</a></li>
        </ul>
      </li>
    <?php 
    }
    else{
    ?>
      <li class="sidebar-active"> <a href="data-adik.php"><i class="fa fa-users"></i> <span>Data Adik</span></a> </li>
      <li class="sidebar-active"> <a href="data-volunteer.php"><i class="fa fa-users"></i> <span>Data Adik</span></a> </li>
      <li class="sidebar-active submenu"> <a href="#"><i class="icon icon-file"></i> <span>Article</span> <span class="label label-important">2</span></a>
        <ul>
          <li><a href="writearticle.php">Write Article</a></li>
          <li><a href="articlelist.php">Article List</a></li>
        </ul>
      </li>
      <li class="sidebar-active"> <a href="showitems.php"><i class="fa fa-shopping-cart"></i> <span>Merchandise</span></a> </li>
    <?php  
    }
    ?>
    
    
    
    
    <li>
      <a>
        <span>
          <form action="include/logout.inc.php" method="POST">
            <i class="icon icon-key"></i> <button type="text" name="logout" class="btn-link">Logout</button>
          </form>
        </span>
      </a> 
    </li>
  </ul>
</div>