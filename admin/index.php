<?php
include 'sidebar.php';
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    <h1>Selamat Datang ka' <?php echo $_SESSION['name']; ?></h1>
  </div>
  <div class="container-fluid">
    <hr>
    <br><br>
    <?php
      if($_SESSION['role'] == "admin"){
    ?>
    <a href="new-user.php"><button type="submit" class="btn btn-primary" name="submit" style="cursor: pointer;">Add New User</button></a>
    <?php
    }
    ?>
    <a href="edit-pass.php"><button type="submit" class="btn btn-primary" name="submit" style="cursor: pointer;">Change Password</button></a>
    <a href="edit-user.php"><button type="submit" class="btn btn-primary" name="submit" style="cursor: pointer;">Edit Info</button></a>
   
  </div>
</div>
<?php
include 'footer.php';
?>