<?php
include 'sidebar.php';
?>
<div id="content">
	<div id="content-header">
		<div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#"> Article</a><a href="#" class="current">Write Article</a> </div>
		<h1>Write For Satoe Atap's Blog</h1>
	</div>
	<div class="container-fluid">
		<hr>
		<form action="include/form/inputartikelP.php" method="post" enctype="multipart/form-data">
			<!-- Pilih Gambar : <input type="file" name="image"> -->
			<input type="hidden" name="author" value="<?php echo $_SESSION['name'] ?>">
			<div class="form-group">
				<label>Judul</label>
				<input class="form-control" type="text" name="title" placeholder="Judul" required style="width: 30%;">
			</div>
			<!-- textarea name="editor1"></textarea>
			<script>
			CKEDITOR.replace( 'editor1' );
			</script> -->
			<div class="form-group">
				<label>Konten</label>
				<textarea class="form-control" id="mytextarea" name="editor1"></textarea>
			</div>
			<input class="btn btn-lg btn-info" type="submit" name="submit" value="Submit">
		</form>
	</div>
</div>
<?php
include 'footer.php';
?>