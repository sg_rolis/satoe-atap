<?php
include 'sidebar.php';
include 'include/Database.inc.php';
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#"> Data Adik</a><a href="#" class="current">Input Data</a> </div>
        <h1>Input Data Adik</h1>
    </div>    
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <form action="include/form/inputadikP.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                            <?php 
                                $query1 = mysqli_query($conn, "SELECT * FROM adik");
                                $row = mysqli_num_rows($query1);
                                $id = $row+2;
                            ?>
                            <input type="hidden" class="form-control" name="id" value="<?php echo $id ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <legend class="col-form-legend col-sm-3">Jenis Kelamin</legend>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gender" value="laki-laki" checked> Laki-laki
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gender" value="perempuan"> Perempuan
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kelas" class="col-sm-3 col-form-label">Kelas</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kelas" id="kelas">
                                <option value="TK">TK</option>
                                <option value="1 SD">1 SD</option>
                                <option value="2 SD">2 SD</option>
                                <option value="3 SD">3 SD</option>
                                <option value="4 SD">4 SD</option>
                                <option value="5 SD">5 SD</option>
                                <option value="6 SD">6 SD</option>
                                <option value="1 SMP">1 SMP</option>
                                <option value="2 SMP">2 SMP</option>
                                <option value="3 SMP">3 SMP</option>
                                <option value="1 SMA/SMK">1 SMA/SMK</option>
                                <option value="2 SMA/SMK">2 SMA/SMK</option>
                                <option value="3 SMA/SMK">3 SMA/SMK</option>
                                <option value="-"> - </option>
                                <option value="Tidak Sekolah">Tidak Sekolah</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <legend class="col-form-legend col-sm-3">Spot</legend>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="spot" value="seroja" checked> Seroja
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="spot" value="tanggul"> Tanggul
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="gambar" class="col-sm-3 col-form-label">Gambar</label>
                        <div class="col-sm-9">
                            <input type="file" name="gambar" id="gambar" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary" name="submit" style="cursor: pointer;">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="col-md-6 status-upload">
                <?php if (isset($_GET['status'])) { ?>
                <p class='bg bg-success text-center'>Selamat data adik berhasil di upload</p>
                <div class="container">
                <div class="row">
                <?php 
                    $status = $_GET['status'];
                    if ($status == 'success') {
                        $query2 = mysqli_query($conn, "SELECT * FROM adik ORDER BY no DESC LIMIT 1;");
                        $hasil = mysqli_fetch_array($query2, MYSQL_ASSOC);
                        echo "<div class='col-sm-4'><img src='".$hasil['img_path'].$hasil['img_name']."' class='img-thumbnail img-fluid'></div>";
                        echo "<div class='col-sm-8 p-t-15'>";
                        echo "<p><strong>Nama: </strong>".$hasil['nama']."</p>";
                        echo "<p><strong>Spot: </strong>".$hasil['spot']."</p>";
                        echo "<p><strong>Gender: </strong>".$hasil['jk']."</p>";
                        echo "<p><strong>Kelas: </strong>".$hasil['kelas']."</p>";
                        echo "</div>";

                    } elseif ($status == 'fail') {
                        echo "<p>Apprantly there is problems while sending your data, please contact developer</p>";
                    }
                ?>
                <div class="col-sm-12">
                    <a href="#" class="btn btn-info">Edit</a>
                    <a href="#" class="btn btn-warning">Delete</a>
                </div>
                </div>
                </div>
                <?php  } ?>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>