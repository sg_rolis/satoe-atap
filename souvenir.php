<?php
include 'header.php';
include 'admin/include/Database.inc.php';
?>
<div class="page-title">
	<div class="col-xs-12 breadcrumb-bar">
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Merchandise</li>
		</ol>
	</div>
	<h1>Merchandise</h1>
</div>
<section class="padding no-title text-center">
	<div class="container">
		<div class="row">
			<!-- <h4>Halaman Lagi Momon Konstruksi Dulu Ya Kak</h4>
			<p>Mohon Bersabar Ini Ujian</p>
			<div class="col-xs-12 col-sm-12 text-center">
				<img class="img-responsive align-center" alt="" src="img/3d/3d-box-1.png"/>
			</div> -->
			<div class="the-blog col-md-12">
				<?php
				$query = mysqli_query($conn, "select * from souvenir");
				// == PAGINATION START HERE
				// define how many item in one page
				$results_per_page = 3;

				// Find the number blog stored in database
				$number_of_article =  mysqli_num_rows($query);
				$number_of_pages = ceil($number_of_article/$results_per_page);

				// Determine which page number visitor currently on
				if (!isset($_GET['page'])) {
					$page = 1;
				}else {
					$page = $_GET['page'];
				}

				// Determine the sql start limit on each page
				$page_first_limit = ($page-1) * $results_per_page;

				// Retriving data based on selected result
				$sql = "SELECT * FROM souvenir LIMIT " . $page_first_limit . ',' . $results_per_page;
				$result = mysqli_query($conn, $sql);
				//END PAGINATION
				?>

				<ul class="souvenir-container clearfix">
				<?php
                while ($product = mysqli_fetch_array($result)) {
                ?>
            		<li class="col-md-4">
            			<div class="item-container">
            				<!-- <div class="product-img" style="background-image: url(<?php echo $product["image"];; ?>);"> -->
            				<div class="product-img" style="background-image: url();">
	            			</div>
	            			<div class="product-img-inner text-center transition">
	            				<!-- <button class="btn btn-info transition">BELI</button> -->
	            				<a href="single-item.php?id=<?php echo $product["no"]; ?>"><button class="btn btn-warning transition">MORE INFO</button></a>
	            			</div>
	            			<div class="product-info">
	            				<b><?php echo strtoupper($product["nama"]); ?></b>
			            		<p class="desc"><?php echo $product["info"]; ?></p>
			            		<p><?php echo "Rp ".$product["harga"]; ?></p>
	            			</div>
            			</div>
            		</li>
         		<?php
         		} 
         		?>
         		</ul>
         		<!-- PAGINATION DISPLAY -->
         		<div class="col-sm-12 pagination">
         			<?php 
         			for ($page=1; $page<=$number_of_pages ; $page++) { 
         					echo "<a href='souvenir.php?page=$page' class='btn btn-link'>".$page."</a>";
         				}
         			 ?>
         		</div>
			</div>
		</div>
	</div>
</section>
<?php
include 'footer.php';
?>